"""
Module for translating the parameters into the pcs format required by SMAC.
"""
import json
import tempfile
import re
from typing import Dict, Optional, List
import logging
from ConfigSpace import ConfigurationSpace
from ConfigSpace.read_and_write import pcs_new

import hos.database as db

_PCS = Dict[str, Dict[str, str]]
_OPT = Dict[str, str]

# Int and Real have [] instead of {} in range
PCS_FORMAT_STR_RANGE = "%s %s {%s} [%s]"
PCS_FORMAT_STR_INTERVAL = "%s %s [%s] [%s]"

LIST_OPTIONS = ["inst_lit_sel", "inst_sos_sth_lit_sel", "abstr_ref"]
NESTED_LIST_OPTIONS = [
    "res_passive_queues",
    "inst_passive_queues",
    "sup_passive_queues",
]
INDEX_OPTIONS = [
    "sup_indices_passive",
    "sup_indices_active",
    "sup_indices_immed",
    "sup_indices_input",
    "sup_light_fw",
    "sup_full_fw",
    "sup_immed_fw_main",
    "sup_immed_fw_immed",
    "sup_input_fw",
    "sup_light_bw",
    "sup_full_bw",
    "sup_immed_bw_main",
    "sup_immed_bw_immed",
    "sup_input_bw",
    "sup_light_triv",
    "sup_full_triv",
    "sup_immed_triv",
    "sup_input_triv",
]

log = logging.getLogger()


def load_raw_pcs_file(pcs_path: str) -> Dict[str, List[Dict[str, str]]]:
    with open(pcs_path) as f:
        pcs_spec = json.load(f)
    return pcs_spec  # type: ignore


def get_pcs_from_json(pcs_path: str, freeze: Optional[List[str]] = None) -> _PCS:
    pcs_spec = load_raw_pcs_file(pcs_path)
    if freeze is None:
        freeze = []

    res = {}
    for pcs_section, options_list in pcs_spec.items():
        # Check if we are freezing this section or not
        # TODO This is not how freezing works!!
        if pcs_section not in freeze:
            # Add all the options in this list to the dict
            for opt in options_list:
                res[opt["opt_name"]] = opt
    return res


def update_matching_options(json_pcs: _PCS, default_options: _OPT) -> _PCS:
    for opt in list(default_options.keys()):
        if opt in json_pcs:
            # Update the value
            json_pcs[opt]["default_val"] = default_options[opt]
            # Remove from dict to indicate that it has been completed
            del default_options[opt]
    return json_pcs


def update_sine_options(json_pcs: _PCS, default_options: _OPT) -> _PCS:
    if (
        "--clausifier_options" not in default_options
        or "-ss" not in default_options["--clausifier_options"]
    ):
        # Check if SInE is being used
        return json_pcs

    claus_val = default_options["--clausifier_options"]
    del default_options["--clausifier_options"]

    # Set the clausifier flag
    if "-ss" in claus_val and "-ss off" not in claus_val:
        sine_flag = "true"
    else:
        sine_flag = "false"
    json_pcs["--clausifier_sine_flag"]["default_val"] = sine_flag

    if sine_flag == "false":
        # Not bothering with the extra options if SInE is off
        return json_pcs

    # Extract the sine parameters and set the values (depth and tolerance)
    for sine_param in ["sd", "st"]:
        # Get the parameter and value
        param_val = re.findall(rf"-{sine_param} \d+\.?\d*", claus_val)[0]
        # Get the value
        param_val = re.findall(r"\d+\.?\d*", param_val)[0]

        # Set the values
        json_pcs[f"--clausifier_sine_{sine_param}"]["default_val"] = param_val

    return json_pcs


def update_list_options(json_pcs: _PCS, default_options: _OPT, option_tag: str) -> _PCS:
    if "--" + option_tag not in default_options:
        return json_pcs

    # Extract the options
    vals = default_options[f"--{option_tag}"][1:-1].split(";")
    del default_options[f"--{option_tag}"]

    # Get the number of values
    no_vals = 0 if vals == [""] else len(vals)

    # Set the value for the length of the list
    json_pcs[f"--{option_tag}_len"]["default_val"] = str(no_vals)

    # Nothing more to do if returning now
    if no_vals == 0:
        return json_pcs

    # Update the values of each list metric
    for i, v in enumerate(vals, start=1):
        json_pcs[f"--{option_tag}_metric_{i}"]["default_val"] = v

    return json_pcs


def update_nested_list_options(
    json_pcs: _PCS, default_options: _OPT, option_tag: str
) -> _PCS:
    # Check if the section is specified
    if "--" + option_tag not in default_options:
        return json_pcs

    # Extract and the freq values
    freq_vals = default_options[f"--{option_tag}_freq"][1:-1].split(";")
    del default_options[f"--{option_tag}_freq"]
    # Get the number of outer queues from the frequency and set the length
    json_pcs[f"--{option_tag}_len"]["default_val"] = str(len(freq_vals))
    # Update the frequencies
    for i, v in enumerate(freq_vals, start=1):
        json_pcs[f"--{option_tag}_{i}_freq"]["default_val"] = v

    # Extract the options
    queue_metrics = re.findall(r"\[(.*?)]", default_options[f"--{option_tag}"])
    del default_options[f"--{option_tag}"]
    # Extract each metric in the queue
    queue_metrics = [q.strip("]").strip("[").split(";") for q in queue_metrics]
    # Update the values of each queue
    for i, q in enumerate(queue_metrics, start=1):
        json_pcs[f"--{option_tag}_{i}_len"]["default_val"] = str(len(q))
        for n, q_val in enumerate(q, start=1):
            json_pcs[f"--{option_tag}_metric_{i}_{n}"]["default_val"] = q_val

    return json_pcs


def update_index_options(
    json_pcs: _PCS, default_options: _OPT, option_tag: str
) -> _PCS:
    # Check if the section is specified
    if "--" + option_tag not in default_options:
        return json_pcs

    ind_vals = default_options[f"--{option_tag}"][1:-1].split(";")
    del default_options[f"--{option_tag}"]
    # Extract all index options for this option tag
    indexes = [ind for ind in json_pcs.keys() if option_tag in ind]

    # Set the index flag
    json_pcs[f"--{option_tag}_flag"]["default_val"] = (
        "false" if ind_vals == [""] else "true"
    )

    # Get all the indexes - set to true if occurring in the value string
    for ind in indexes:
        ind_tag = ind.split("_")[-1]
        if ind_tag == "flag":
            # The flag is irrelevant for this
            continue
        if ind_tag in ind_vals:
            json_pcs[ind]["default_val"] = "true"
        else:
            json_pcs[ind]["default_val"] = "false"
    return json_pcs


def remove_known_non_optimisation_values(default_options: _OPT):
    """
    Remove options from the dict that are generally not optimised
    """
    opts = [
        "--schedule",
        "--clausifier",
        "--out_options",
        "--stats_out",
        "--clausifier_options",
        "--time_out_real",
        "--proof_out",
        "--preprocessed_out",
    ]
    for opt in opts:
        if opt in default_options:
            del default_options[opt]


def update_special_options(json_pcs: _PCS, default_options: _OPT) -> _PCS:
    """
    Parse some special options that cause issues if not handled explicitly.
    """
    if "--abstr_ref_under" in default_options:
        if default_options["--abstr_ref_under"] == "[cone]":
            json_pcs["--abstr_ref_under"]["default_val"] = "cone"
        elif default_options["--abstr_ref_under"] == "[]":
            json_pcs["--abstr_ref_under"]["default_val"] = "none"
        else:
            raise ValueError(
                f"Handling of value {default_options['--abstr_ref_under']} not implemented for abstr_ref_under"
            )
        del default_options["--abstr_ref_under"]

    return json_pcs


def update_default_values(json_pcs: _PCS, incumbent: int) -> _PCS:
    """
    Updates the default values of the pcs given the options used in a previous experiment in the database.
    We assume that all experiment options exist in parameter space.
    """
    # Get the incumbent options for the experiment
    new_defaults_list = db.experiment.get_experiment_options(incumbent)
    # Convert to dict
    new_defaults = {k: str(v) for (k, v) in new_defaults_list}

    # Update the special cases first
    json_pcs = update_special_options(json_pcs, new_defaults)
    # Update the basic options with matching names
    json_pcs = update_matching_options(json_pcs, new_defaults)
    # Extract and update sine values if present
    json_pcs = update_sine_options(json_pcs, new_defaults)

    # Update the rest of the value types; list, nested list, indexes
    for option_tag in LIST_OPTIONS:
        json_pcs = update_list_options(json_pcs, new_defaults, option_tag)
    for option_tag in NESTED_LIST_OPTIONS:
        json_pcs = update_nested_list_options(json_pcs, new_defaults, option_tag)
    for option_tag in INDEX_OPTIONS:
        # This was slow - but new more general implementation is slow...
        json_pcs = update_index_options(json_pcs, new_defaults, option_tag)

    remove_known_non_optimisation_values(new_defaults)

    # Check that default
    if len(new_defaults) > 0:
        log.warning(
            f"{len(new_defaults)} default options not set in the parameter configuration. Values: {new_defaults}"
        )

    return json_pcs


def convert_json_to_pcs_string(json_pcs: _PCS) -> str:
    """
    Convert the json parameter description into a pcs string.
    """

    pcs_string = ""
    for opt_conf in json_pcs.values():
        if opt_conf["opt_type"] in ["real", "integer"]:
            opt_format = PCS_FORMAT_STR_INTERVAL
        else:
            opt_format = PCS_FORMAT_STR_RANGE
        opt_string = opt_format % (
            opt_conf["opt_name"][2:],
            opt_conf["opt_type"],
            opt_conf["opt_domain"],
            opt_conf["default_val"],
        )

        if opt_conf["possible_condition"] != "":
            opt_string += "\n" + opt_conf["possible_condition"]

        pcs_string += opt_string + "\n"

    # Return file path
    return pcs_string


def load_pcs_string_as_configuration(pcs_string: str) -> ConfigurationSpace:
    tmp = tempfile.NamedTemporaryFile()
    with open(tmp.name, "r+") as f:
        f.write(pcs_string)
        f.seek(0)
        config_pcs = pcs_new.read(f)
    return config_pcs


def get_smac_configuration_space(
    pcs_path: str, incumbent: Optional[int], parameter_freeze: Optional[List[str]]
) -> ConfigurationSpace:
    # Load pcs
    json_pcs = get_pcs_from_json(pcs_path, parameter_freeze)

    # Update the default options to the incumbent if provided
    if incumbent is not None:
        json_pcs = update_default_values(json_pcs, incumbent)

    # TODO remove _ TMPFIX
    log.warning("Converting all ordinal values to integer/real as a tmp fix")
    for k, v in json_pcs.items():
        if v["opt_type"] == "ordinal":
            try:
                v["opt_type"] = "integer"
                min_val = min(map(int, v["opt_domain"].split(",")))
                max_val = max(map(int, v["opt_domain"].split(",")))
            except ValueError:
                v["opt_type"] = "real"
                min_val = min(map(float, v["opt_domain"].split(",")))  # type: ignore
                max_val = max(map(float, v["opt_domain"].split(",")))  # type: ignore
            v["opt_domain"] = f"{min_val}, {max_val}"

    # Convert to string
    pcs_string = convert_json_to_pcs_string(json_pcs)

    # Load as config space
    config_pcs = load_pcs_string_as_configuration(pcs_string)

    return config_pcs
