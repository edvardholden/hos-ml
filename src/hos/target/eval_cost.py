"""
Module for computing the evaluation cost (quality) of a heuristic over a given problem set.
"""
from enum import Enum
from typing import Dict, List
import logging

import hos.database as db

log = logging.getLogger()

# The weight for unique problems in the weighted_solved metric
UNIQUE_SOLVED_WEIGHT: int = 30


class QualityMetric(Enum):
    TOTAL_SOLVED = "total_solved"
    TOTAL_TIME = "total_time"
    WEIGHTED_SOLVED = "weighted_solved"


def compute_scores(experiment_id: int) -> Dict[str, int]:
    quality_dict = {
        "total_solved": db.stats.query_no_solved_problems_in_exp(experiment_id),
        "total_time": int(db.stats.query_tot_time_in_exp(experiment_id)),
        "weighted_solved": compute_weighted_solved(experiment_id),
    }
    return quality_dict


def compute_weighted_solved(experiment_id: int):
    # Get the initial incumbent in this smac experiment
    incumbent = db.smac.get_current_smac_experiment_incumbent()

    # If the incumbent is not defined, set it as empty
    if incumbent is None:
        solved_incumbent = []
        log.warning(
            'eval:cost - Could not get the current smac incumbent when computing cost "weighted_solved"'
        )
    else:
        # Get the problems solved in the incumbent
        solved_incumbent = db.stats.query_id_solved_problems_in_exp(incumbent)

    # Get problems solved in the experiment
    solved_this = db.stats.query_id_solved_problems_in_exp(experiment_id)

    # Compute the score based on the solved sets
    score = weighted_solved_metric(solved_incumbent, solved_this)

    # Return the score
    return score


def weighted_solved_metric(solved_incumbent: List[str], solved_experiment: List[str]) -> int:
    # Compute weighted score: len(B) + w * (B - A)
    score = len(solved_experiment) + UNIQUE_SOLVED_WEIGHT * len(
        set(solved_experiment).difference(set(solved_incumbent))
    )
    return score


def compute_quality_scores(experiment_id: int) -> tuple[str, int]:
    # Query the various metrics and store them in a dict entry
    quality_dict = compute_scores(experiment_id)

    # Insert the quality measures to the DB
    db.smac.insert_quality_scores(experiment_id, quality_dict)

    # Get the smac quality measure
    measure = db.smac.get_current_quality_measure()

    cost = score_to_cost(measure, quality_dict[measure])
    return measure, cost


def score_to_cost(measure: str, score: int) -> int:
    # Convert to negative for cost minimisation
    if measure == "total_time":
        return score

    # Negated case
    return -score
