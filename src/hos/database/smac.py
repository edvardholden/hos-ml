"""
Module containing common queries related to handling SMAC specifics.
"""
from typing import Dict, List

from hos.database.base import query_commit, query_select


def insert_param_string(param_dict: Dict[str, str], experiment_id: int) -> None:
    # Reconstruct the arg string - dicts are order preserving
    arg_str = " ".join(f"--{k}={v}" for k, v in param_dict.items())
    query = f"""
                 INSERT INTO SMAC_ParamString (iProverExperimentID, ParamString, ParamFreeze)
                 VALUES({experiment_id}, "{arg_str}", (SELECT Parameters
                                     FROM SMAC_ParameterFreeze
                                     WHERE SMACExperimentID = (SELECT MAX(SMACExperimentID)
                                                           FROM SMACExperiment))
                       )
                 ;"""
    query_commit(query)


def insert_wrapper_run(iprover_experiment: int, quality: float, runtime: float) -> None:
    """Inserts the new wrapper run into the db."""
    query = f"""
        INSERT INTO WrapperRun (Runtime,
                                Quality,
                                SMACExperiment,
                                Experiment,
                                PossibleProblem)
        VALUES ({runtime}, {quality}, (SELECT MAX(SMACExperimentID) FROM SMACExperiment), {iprover_experiment}, 0)
        ;"""
    query_commit(query)


def check_experiment_is_parallel() -> bool:
    query = """
                 SELECT *
                 FROM SMAC_ParallelExperiment
                 WHERE  SMACExperimentID=(SELECT MAX(SMACExperimentID)
                                          FROM SMAC_ProblemCollection)
                 ;"""
    res = query_select(query)
    return bool(res != [])


def get_current_smac_experiment_incumbent():
    # Get the cpu time of the experiment
    query = """
             SELECT Incumbent
             FROM SMAC_QualityFunction
             WHERE SMACExperimentID = (SELECT MAX(SMACExperimentID) FROM SMACExperiment)
            ;"""

    res = query_select(query)
    return res[0][0]


def insert_quality_scores(iprover_exp: int, scores: Dict[str, int]) -> None:
    """Inserts all computed quality measures into the apt table in the db."""

    query = f"""
            INSERT INTO SMAC_QualityMeasures (SMACExperimentID,
                                              ExperimentID,
                                              TotalSolved,
                                              TotalTime,
                                              WeightedSolved
                                              )
            VALUES((SELECT MAX(SMACExperimentID) FROM SMACExperiment),
                    {iprover_exp}, {scores["total_solved"]}, {scores["total_time"]}, {scores["weighted_solved"]})
            ;"""
    query_commit(query)


def get_current_quality_measure() -> str:
    """Get the quality measure from the current SMAC experiment."""
    query = """
                 SELECT Measure
                 FROM SMAC_QualityFunction
                 WHERE SMACExperimentID = (SELECT MAX(SMACExperimentID) FROM SMACExperiment)
            ;"""

    res = query_select(query)
    return str(res[0][0])


def setup_smac_experiment_in_db(
    problem_collection: str, initial_heuristics: List[int], pcs_file: str
) -> None:
    heuristics = ", ".join(str(h) for h in initial_heuristics)

    query = f"""
                INSERT INTO HLP_Experiment
                (StartTime, ProblemCollection,
                InitialHeuristics, ExperimentPCSFile)
                VALUES (NOW(), '{problem_collection}', '{heuristics}', '{pcs_file}')
            ;"""

    query_commit(query)


def insert_hlp_evaluation_statistics(
    global_heuristics: List[int],
    solved_union: int,
    solved_intersection_avg_time: float,
    solved_complement_to_default: int,
    solved_problems_best_heuristic: int,
) -> None:
    query = f"""
                 INSERT INTO HLP_RunMeasurement
                 (HLPRunID, ExperimentID, DefaultHeuristic, GlobalHeuristics, SolvedUnion,
                  SolvedInterAvgTime, SolvedComplement, SolvedBestHeuristic)
                 VALUES ((SELECT MAX(HLPRunID) FROM HLP_Run),
                         (SELECT MAX(ExperimentID) FROM HLP_Experiment),
                         Null,
                         "{', '.join(str(h) for h in global_heuristics)}",
                        "{solved_union}",
                        "{solved_intersection_avg_time}",
                        "{solved_complement_to_default}",
                        "{solved_problems_best_heuristic}"
                         )
                 ;"""

    query_commit(query)


def insert_smac_param_string_evaluation(global_id: int, local_id: int):
    """
    Insert SMAC parameters for this experiment into the paramstring table.
    """

    query_select_params = f"""
                 SELECT ParamString
                 FROM SMAC_ParamString
                 WHERE iProverExperimentID="{local_id}"
                 ;"""
    param_string = query_select(query_select_params)
    param_string = param_string[0][0]

    query_select_freeze = f"""
                 SELECT ParamFreeze
                 FROM SMAC_ParamString
                 WHERE iProverExperimentID="{local_id}"
                 ;"""
    param_freeze = query_select(query_select_freeze)

    # Insert the string with the iProver experiment id of the evaluation
    query_insert = f"""
                 INSERT INTO SMAC_ParamString(iProverExperimentID, ParamString, ParamFreeze)
                 VALUES ("{global_id}", "{param_string}","{param_freeze}")
                 ;"""
    query_commit(query_insert)


def report_smac_experiment_end_time(smac_exp_id: int) -> None:
    query = f"""UPDATE SMACExperiment
                SET SMACExperimentEndTime = NOW()
                WHERE SMACExperimentID = {smac_exp_id}
                ;"""
    query_commit(query)


def initialise_smac_experiment_db(smac_version, scenario, pcs_file) -> int:
    # Report the data to the DB
    insert_query = f"""
                 INSERT INTO SMACExperiment
                        (SMACVersion, SMACExperimentScenario, SMACExperimentPCS, SMACExperimentStartTime)
                        VALUES ("{smac_version}", "{smac_version}", "{pcs_file}", NOW())
                ;"""
    query_commit(insert_query)

    # Get the smac experiment ID
    select_query = "SELECT MAX(SMACExperimentID) FROM SMACExperiment"
    res = query_select(select_query)
    return int(res[0][0])


def report_quality_function(smacexp_id: int, quality, incumbent: int):
    # Set current quality function for SMAC
    query = f"""INSERT INTO SMAC_QualityFunction (SMACExperimentID,
                                                        Measure,
                                                        Incumbent)
            VALUES({smacexp_id}, '{str(quality)}', {incumbent})
            ; """
    query_commit(query)


def report_problem_collection(smacexp_id: int, collection: str):
    query = f"""
                 INSERT INTO SMAC_ProblemCollection(SMACExperimentID,
                                                    ProblemCollection)
                 VALUES("{smacexp_id}", "{collection}")
                 ;"""
    query_commit(query)
