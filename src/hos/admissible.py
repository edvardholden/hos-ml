"""
Module containing the admissible definition and clustering approach.
"""
import multiprocessing
import os
import warnings
from enum import Enum
import random
from typing import List, Optional, Tuple, Dict
import pandas as pd
import logging
import numpy as np
import scipy.sparse as sp
from kneed import KneeLocator
from sklearn.metrics import pairwise_distances
from sklearn.metrics import silhouette_score, calinski_harabasz_score
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    # Filter Deprecation warning on how package version is checked
    from sklearn_extra.cluster import KMedoids

import hos.experiment_config as exp_conf
import hos.database as db
from hos.embedding import compute_unsolved_problem_embeddings, TargetType

log = logging.getLogger()

_CLUSTERS = List[List[int]]
_ACLUSTERS = Dict[int, List[int]]

MIN_ADMISSIBLE_HEURISTIC = 4
MIN_ADMISSIBLE_CLUSTER_POPULATION = 14
ADMISSIBLE_SAMPLE_UNSOLVED_RATIO = 2.0


class ClusterQualityMetric(str, Enum):
    DI = "distortion"
    CH = "calinski"
    SI = "silhouette"


class UnsolvedMapping(str, Enum):
    NONE = "none"
    SAMPLING = "random_sampling"
    KNEAREST = "knn_classification"
    KMEDOID = "kmedoid_classification"
    DISTANCE = "embedded_distance"


def get_admissible_clusters(
    heuristics: List[int],
    problem_collection: str,
    admissible_constant: float,
    admissible_factor: float,
    admissible_filter_ratio: float,
    admissible_quality_metric: ClusterQualityMetric,
    admissible_unsolved_mapping: UnsolvedMapping,
    admissible_unsolved_target: TargetType,
    admissible_unsolved_sampling_ratio: float,
    admissible_unsolved_n_neighbours: int,
) -> _CLUSTERS | None:
    if len(heuristics) < MIN_ADMISSIBLE_HEURISTIC:
        log.warning(
            f"Less than {MIN_ADMISSIBLE_HEURISTIC} available heuristics, not clustering on admissibility"
        )
        return None

    # This is what should be called on what should minister this admissible part
    admissible_clusters, kmedoid_classifier, df_eval = compute_admissible_clusters(
        heuristics,
        problem_collection,
        admissible_constant=admissible_constant,
        admissible_factor=admissible_factor,
        admissible_filter_ratio=admissible_filter_ratio,
        cluster_quality_metric=admissible_quality_metric,
    )

    # Admissible clustering occurs on solved problems, supplement with unsolved via e.g. embedding distance
    admissible_clusters = supplement_unsolved_problems(
        admissible_clusters,
        admissible_unsolved_mapping,
        problem_collection,
        df_eval,
        admissible_constant,
        admissible_factor,
        admissible_unsolved_target,
        admissible_unsolved_sampling_ratio,
        kmedoid_classifier,
        admissible_unsolved_n_neighbours,
    )

    # Filter clusters based on size
    admissible_clusters = remove_small_admissible_clusters(admissible_clusters)

    # Convert the cluster dictionary to a cluster list - in a random fashion
    clusters = [admissible_clusters[k] for k in admissible_clusters.keys()]
    return clusters


def get_problem_ids_not_in_clusters(clusters: _ACLUSTERS, problem_collection: str):
    # Get hold of the unsolved instances
    problem_ids = db.general.get_problem_ids_in_collection(problem_collection)
    clustred_ids = [prob for cluster in list(clusters.values()) for prob in cluster]
    unclustered_ids = sorted(set(problem_ids) - set(clustred_ids))
    return unclustered_ids


def supplement_unsolved_problems(
    clusters: _ACLUSTERS,
    mapping: UnsolvedMapping,
    problem_collection: str,
    df_eval: pd.DataFrame,
    adm_sc: float,
    adm_sf: float,
    target_type: TargetType,
    unsolved_sampling_ratio: float,
    kmedoid_classifier: KMedoids,
    n_neighbours: int,
) -> _ACLUSTERS:
    if mapping is UnsolvedMapping.NONE:
        # Do not supplement the clusters with any unsolved problems
        return clusters

    # Get non cluster problems
    supplement_ids = get_problem_ids_not_in_clusters(clusters, problem_collection)
    if len(supplement_ids) == 0:
        log.warning("No supplemental instances for supplementing the clusters")
        return clusters

    if mapping is UnsolvedMapping.SAMPLING:
        return supplement_random_unsolved_problems(clusters, supplement_ids, ADMISSIBLE_SAMPLE_UNSOLVED_RATIO)

    # Compute the admissible embeddings of the remaining instances
    id_map = db.general.get_filename_from_problem_ids_map(
        list(df_eval.index), problem_collection, reverse=True
    )
    df_eval_filenames = df_eval.rename(index=id_map)

    embeddings = compute_unsolved_problem_embeddings(
        problem_collection,
        df_eval_filenames,
        target_type,
        adm_sc,
        adm_sf,
    )
    if len(embeddings) == 0:
        log.warning("No problems retrieved from embedding computation.")
        return clusters

    # Convert to dataframe with the correct id indexes
    emb_df = convert_embedding_dict_to_df_with_ids(embeddings, problem_collection)

    if mapping is UnsolvedMapping.KNEAREST:
        adm_df = compute_admissible_evaluation_matrix(df_eval_filenames, adm_sc, adm_sf)
        clusters = supplement_knn_classification(clusters, kmedoid_classifier, n_neighbours, emb_df, adm_df)
    elif mapping is UnsolvedMapping.KMEDOID:
        clusters = supplement_kmedoid_classification(clusters, kmedoid_classifier, emb_df)
    elif mapping is UnsolvedMapping.DISTANCE:
        clusters = supplement_embedded_distance(
            clusters,
            kmedoid_classifier.cluster_centers_,
            emb_df,
            unsolved_sampling_ratio,
        )
    else:
        raise ValueError(f'No unsolved supplementing function implemented for mapping "{mapping}\'"')

    return clusters


def convert_embedding_dict_to_df_with_ids(
    embeddings: Dict[str, List[List[float]]], problem_collection: str
) -> pd.DataFrame:
    # Convert to dataframe
    emb_df = pd.DataFrame(embeddings).T

    # Convert problem names to ids as the ids are used to create collections
    id_map = db.general.get_filename_problem_id_map(list(emb_df.index), problem_collection)
    emb_df = emb_df.rename(index=id_map)

    return emb_df


def supplement_random_unsolved_problems(
    clusters: _ACLUSTERS, supplement_ids: List[int], sample_ratio: float
) -> _ACLUSTERS:
    """
    Supplements the clusters with ratio*cluster_size unsolved problems.
    Any appearing not appearing in the cluster is deemed as unsolved.
    """
    for cluster in clusters:
        # Sample and supplement the problem cluster with the additional problems
        supp_size = min(len(supplement_ids), int(len(clusters[cluster]) * sample_ratio))
        clusters[cluster] += random.sample(supplement_ids, supp_size)  # type: ignore

    # Return the new extended clusters
    return clusters


def supplement_kmedoid_classification(
    clusters: _ACLUSTERS, kmedoid_classifier: KMedoids, emb_df: pd.DataFrame
) -> _ACLUSTERS:
    """
    Supplement problems by predicting their appropriate cluster via the KMedoids classifier.
    """

    # Map the embeddings to the clusters
    supp_cluster_dict = map_predict_problems_to_clusters(kmedoid_classifier, emb_df)

    # Merge the clusters
    for k, v in supp_cluster_dict.items():
        clusters[k] = clusters.get(k, []) + v
    return clusters


def supplement_embedded_distance(
    clusters: _ACLUSTERS,
    cluster_centers: List[List[int]],
    emb_df: pd.DataFrame,
    sampling_ratio: float,
) -> _ACLUSTERS:
    """
    # Sample sampling_ratio problems by their distance to the cluster medoid.
    """
    # Compute indices between cluster centers and each embedded point
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        pair_dist = pairwise_distances(emb_df.values, Y=np.asarray(cluster_centers), metric=common_solved)
    # Sort indexes according to their distance
    min_dist = np.argsort(pair_dist, axis=0).T

    # Sample based on distance and ratios
    for cid, probs in clusters.items():
        sample_len = min(int(len(probs) * sampling_ratio), len(emb_df.index))

        sample_indexes = min_dist[cid][:sample_len]
        samples = list(emb_df.iloc[sample_indexes].index)
        clusters[cid] = probs + samples

    return clusters


def supplement_knn_classification(
    clusters: _ACLUSTERS,
    kmedoid_classifier: KMedoids,
    n_neighbours: int,
    emb_df: pd.DataFrame,
    adm_df: pd.DataFrame,
) -> _ACLUSTERS:
    # Initialise knn model based on the historical data
    neigh = KNeighborsClassifier(n_neighbors=n_neighbours, metric=common_solved)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        neigh.fit(adm_df, kmedoid_classifier.predict(emb_df))
        # Predict the labels of the embeddings
        labels = neigh.predict(emb_df)

    # Supplement into the clusters
    for prob, label in zip(emb_df.index, labels):
        clusters[label].append(prob)
    return clusters


def compute_admissible_clusters(
    heuristics: List[int],
    problem_collection: str,
    admissible_constant: float,
    admissible_factor: float,
    admissible_filter_ratio: float,
    cluster_quality_metric: ClusterQualityMetric,
) -> Tuple[_ACLUSTERS, KMedoids, pd.DataFrame]:
    # Get hold of evaluation matrix and problem metadata
    eval_df = get_heuristic_evaluation_data(heuristics, problem_collection, exp_conf.get_global_timeout())

    # Compute the admissible evaluation
    adm_df = compute_admissible_evaluation_matrix(eval_df, admissible_constant, admissible_factor)

    # Cluster on the admissible evaluation data
    kmedoid_classifier = compute_kmedoids_classifier(adm_df, admissible_filter_ratio, cluster_quality_metric)
    log.debug(f"Medoids: {kmedoid_classifier.cluster_centers_}")

    # Predict solved clusters and get the labels
    solved_cluster_dict = map_predict_problems_to_clusters(kmedoid_classifier, adm_df)

    return solved_cluster_dict, kmedoid_classifier, eval_df


def remove_small_admissible_clusters(cluster_dict: _ACLUSTERS) -> _ACLUSTERS:
    removed_counter = 0

    for k in list(cluster_dict.keys()):
        if len(cluster_dict[k]) < MIN_ADMISSIBLE_CLUSTER_POPULATION:
            del cluster_dict[k]
            removed_counter += 1

    if removed_counter > 0:
        log.info(
            f"Removed {removed_counter} clusters with population less than {MIN_ADMISSIBLE_CLUSTER_POPULATION}"
        )

    return cluster_dict


def map_predict_problems_to_clusters(clf: KMedoids, adm_df: pd.DataFrame) -> Dict[int, List[int]]:
    cluster_dict: Dict[int, List[int]] = {n: [] for n in range(clf.n_clusters)}
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        labels = clf.predict(adm_df.values)

    for prob, label in zip(adm_df.index, labels):
        cluster_dict[label].append(prob)

    return cluster_dict


def common_solved(u, v, **kwargs):
    """
    Computes the admissible similarity between admissible evaluation features.
    Based on Dice similarity.
    """

    u = np.asarray(u, dtype=np.float32)
    v = np.asarray(v, dtype=np.float32)

    if len(u.shape) == 1:
        u = np.asarray([u], dtype=np.float32)

    if len(v.shape) == 1:
        v = np.asarray([v], dtype=np.float32)

    # Solves both
    common = np.sum(np.multiply(u, v), axis=1) * 2

    # Compute number of solved
    solved = np.sum(u, axis=1) + np.sum(v, axis=1)

    with np.errstate(divide="ignore", invalid="ignore"):
        ratio = np.nan_to_num(common / solved, nan=0.0)

    dist = 1.0 - ratio
    return dist


def get_heuristic_evaluation_data(
    heuristics: List[int], problem_collection: str, runtime_upper_bound: float
) -> pd.DataFrame:
    problem_ids = db.general.get_problem_ids_in_collection(problem_collection)
    log.debug(f"Number of problems: {len(problem_ids)}")

    heuristic_features = []
    for heur in heuristics:
        res = db.stats.query_heuristic_evaluation(heur)

        # Check that the number of problems in the experiment is correct
        if len(res) != len(problem_ids):
            raise ValueError(
                f"Error: Experiment {heur} contains {len(res)} instances "
                f"Error: Experiment {heur} contains {len(res)} instances "
                f"while there are {len(problem_ids)} problems"
            )
        heuristic_features += [res]

    # Transpose the data according to the desired format
    evaluation = np.asarray(heuristic_features).T

    # Construct a dataframe over the evaluation data
    df = pd.DataFrame(evaluation, index=problem_ids, columns=heuristics)

    # Mask high timelimit
    df = mask_runtime_upper_bound_df(df, runtime_upper_bound)

    log.info(
        f"Created dataframe with rows:{len(df.index)} columns:{len(df.columns)} and timeout:{runtime_upper_bound}s"
    )
    return df


def mask_runtime_upper_bound_df(df: pd.DataFrame, runtime_upper_bound: Optional[float]) -> pd.DataFrame:
    if runtime_upper_bound is not None:
        df = df.mask(df > runtime_upper_bound, None)
    return df


def compute_admissible_evaluation_matrix(df: pd.DataFrame, adm_sc: float, adm_sf: float) -> pd.DataFrame:
    # Remove unsolved problems
    df = df.dropna(axis="index", how="all")
    log.debug(f"Problems after removing unsolved: {len(df.index)}")

    # Get the fastest times
    fastest = df.min(axis=1)

    # Compute whether the heuristics are admissible
    df_adm = df.le(fastest + adm_sc, axis=0) | df.le(fastest * adm_sf, axis=0)
    return df_adm


def compute_kmedoids_range(adm_df: pd.DataFrame) -> Tuple[int, int]:
    """
    Compute the range of K values for finding the optimal number of k clusters.
    """
    # Start with two as a base
    k_min = 2

    # Either the number of unique vectors or the number of heuristics, whichever is less.
    k_max = min(len(np.unique(adm_df.values, axis=0)), len(adm_df.columns))
    k_max = int(k_max * 0.7)  # Scale
    k_max = max(k_max, k_min + 2)

    return k_min, k_max


def filter_problems_by_admissible_ratio(adm_df: pd.DataFrame, adm_ratio: float) -> pd.DataFrame:
    """
    Filters (removes) problems that have a number of admissible entries over a certain threshold
    """
    adm_df = adm_df[adm_df.apply(sum, axis=1) <= len(adm_df.columns) * adm_ratio]
    log.debug(f"Feature matrix size after filtering on admissible ratio: {len(adm_df)}")
    return adm_df


def compute_optimal_kmedoid_in_ranges(
    adm_df: pd.DataFrame, k_min: int, k_max: int, metric: ClusterQualityMetric
) -> int:
    # Compute the distance matrix to reduce redundancy
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        X_dist = pairwise_distances(adm_df.values, metric=common_solved, n_jobs=-1)
    log.debug("Computed pairwise distances")

    # Compute the cluster qualities
    quality_dict = compute_cluster_quality_parallel(X_dist, adm_df.values, k_min, k_max)

    # Get the optimal k value from the results
    k_opt = compute_optimal_k_from_quality(quality_dict, metric)
    return k_opt


def compute_kmedoids_classifier(
    adm_df: pd.DataFrame,
    adm_ratio: Optional[float],
    metric: ClusterQualityMetric,
    k_values: Optional[Tuple[int, int]] = None,
) -> KMedoids:
    """
    Computes and returns the classifier for the optimal number of labels.
    """
    if adm_ratio is not None:
        # TODO this could eliminate the whole admissible matrix
        adm_df = filter_problems_by_admissible_ratio(adm_df, adm_ratio)

    if k_values is None:
        # Compute the min and max values
        k_min, k_max = compute_kmedoids_range(adm_df)
    else:
        # Assign min and max values
        k_min, k_max = k_values

    log.info(f"Evaluating k-medoids for values: {k_min} to {k_max}")

    k_opt = compute_optimal_kmedoid_in_ranges(adm_df, k_min, k_max, metric)

    # Compute the cluster ownerships, for the original data such that we can re-use the object for new data
    _, kmedoids = compute_kmedoid_cluster_labels(adm_df, k_opt, metric=common_solved)
    log.debug(f"Computed classifier for k: {k_opt}")

    # Return clusters and cluster centers
    return kmedoids  # kmedoids.cluster_centers_ # FIXME


def compute_cluster_quality_parallel(
    X_dist, X_data, k_min, k_max
) -> Dict[int, Dict[ClusterQualityMetric, float]]:
    # Create tuple of the features and the k values
    k_args = [(X_dist, X_data, i) for i in range(k_min, k_max + 1)]

    # Make a process pool
    cpus = (os.cpu_count() or 3) - 1
    pool = multiprocessing.Pool(processes=cpus)
    log.debug(f"Running processes with {cpus} cpus")

    # Compute the processes
    res = pool.starmap(compute_and_evaluate_kmedoids_quality, k_args)

    # No more processes to the queue
    pool.close()
    # Wait for the pool to finish processing before carrying on
    pool.join()

    # Convert the result into a dict
    quality_dict = dict(res)

    return quality_dict


def evaluate_kmedoids_quality(X_dist, X_data, labels) -> Dict[ClusterQualityMetric, float]:
    if len(set(labels)) < 2:
        log.warning("All problems clustered into the same admissible cluster, quality metrics are undefined.")
        scores = {
            ClusterQualityMetric.SI: float("inf"),
            ClusterQualityMetric.CH: float("inf"),
            ClusterQualityMetric.DI: float("inf"),
        }
        return scores

    # Compute SU score
    si_score = silhouette_score(X_dist, labels, metric="precomputed")
    # si_score = silhouette_score(X_data, labels, metric=common_solved)

    # Compute distortion
    di_score = distortion_score(X_data, labels, metric=common_solved)
    # di_score = distortion_score(X_dist, labels, metric="precomputed")

    # Compute CH score
    hc_score = calinski_harabasz_score(X_data, labels)

    scores = {
        ClusterQualityMetric.SI: si_score,
        ClusterQualityMetric.CH: hc_score,
        ClusterQualityMetric.DI: di_score,
    }
    return scores


def compute_and_evaluate_kmedoids_quality(X_dist, X_data, k) -> Tuple[int, Dict[ClusterQualityMetric, float]]:
    log.debug(f"Computing clusters for k {k}")
    # Compute the cluster labels
    labels, _ = compute_kmedoid_cluster_labels(X_dist, k, metric="precomputed")

    # Evaluate the clustering according to the labels
    scores = evaluate_kmedoids_quality(X_dist, X_data, labels)

    return k, scores


def compute_kmedoid_cluster_labels(X, k, metric=common_solved):
    # Cluster the data
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        kmedoids = KMedoids(k, metric=metric, init="k-medoids++")
        kmedoids.fit(X)

    # Return the labels
    return kmedoids.labels_, kmedoids


def distortion_score(X, labels, metric):
    # From https://github.com/DistrictDataLabs/yellowbrick/blob/3125f4a1973946cd7214e7f6515dd707d75ba86d/
    # yellowbrick/cluster/elbow.py
    """
    Compute the mean distortion of all samples.
    The distortion is computed as the the sum of the squared distances between
    each observation and its closest centroid. Logically, this is the metric
    that K-Means attempts to minimize as it is fitting the model.
    .. seealso:: http://kldavenport.com/the-cost-function-of-k-means/
    Parameters
    ----------
    X : array, shape = [n_samples, n_features] or [n_samples_a, n_samples_a]
        Array of pairwise distances between samples if metric == "precomputed"
        or a feature array for computing distances against the labels.
    labels : array, shape = [n_samples]
        Predicted labels for each sample
    metric : string
        The metric to use when calculating distance between instances in a
        feature array. If metric is a string, it must be one of the options
        allowed by `sklearn.metrics.pairwise.pairwise_distances
        <http://bit.ly/2Z7Dxnn>`_
    """
    # Encode labels to get unique centers and groups
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    unique_labels = le.classes_

    # Sum of the distortions
    distortion = 0

    # Loop through each label (center) to compute the centroid
    for current_label in unique_labels:
        # Mask the instances that belong to the current label
        mask = labels == current_label
        instances = X[mask]

        # Compute the center of these instances
        center = instances.mean(axis=0)

        # NOTE: csc_matrix and csr_matrix mean returns a 2D array, numpy.mean
        # returns an array of 1 dimension less than the input. We expect
        # instances to be a 2D array, therefore to do pairwise computation we
        # require center to be a 2D array with a single row (the center).
        # See #370 for more detail.
        if not sp.issparse(instances):
            center = np.array([center])

        # Compute the square distances from the instances to the center
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            distances = pairwise_distances(instances, center, metric=metric)
        distances = distances**2

        # Add the sum of square distance to the distortion
        distortion += distances.sum()

    return distortion


def compute_knee_point(
    k_values: List[int], k_scores: List[float], metric: ClusterQualityMetric
) -> int | None:
    if metric is ClusterQualityMetric.SI or metric is ClusterQualityMetric.CH:
        curve, direction = "concave", "increasing"
    elif metric is ClusterQualityMetric.DI:
        curve, direction = "convex", "decreasing"
    else:
        raise ValueError(
            f"Curve and direction for knee location not implemented for ClusterQualityMetric: {metric}"
        )

    # Suppress warnings if no knee point is found
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        knee_loc = KneeLocator(
            k_values,
            k_scores,
            curve=curve,
            direction=direction,
            interp_method="polynomial",
            online=True,
        )
        knee_point: int | None = knee_loc.knee
    return knee_point


def compute_optimal_k_from_scores(
    k_values: List[int], k_scores: List[float], metric: ClusterQualityMetric
) -> int:
    if metric is ClusterQualityMetric.SI or metric is ClusterQualityMetric.CH:
        optimal_index = int(np.argmax(k_scores))
    elif metric is ClusterQualityMetric.DI:
        optimal_index = int(np.argmin(k_scores))
    else:
        raise ValueError(
            f"No optimal score direction (min or max) implemented for ClusterQualityMetric: {metric}"
        )
    k_opt = k_values[optimal_index]
    return k_opt


def compute_optimal_k_from_quality(
    quality_dict: Dict[int, Dict[ClusterQualityMetric, float]],
    metric: ClusterQualityMetric,
) -> int:
    # Report which metric type that is in use and extract the scores for the metric
    log.debug(f"Using metric {metric}")
    # Get values in sorted order
    k_values = sorted(quality_dict.keys())
    k_scores = [quality_dict[k][metric] for k in k_values]

    # Attempt to compute the knee point basd on the scores
    knee_point = compute_knee_point(k_values, k_scores, metric)
    if knee_point is not None:
        # Return the knee point as the optimal K if found.
        log.info(f"Found knee point for K: {knee_point} with score: {quality_dict[knee_point][metric]:.2f}")
        return knee_point

    log.info("No knee point, computing optimal K based on score")
    k_opt = compute_optimal_k_from_scores(k_values, k_scores, metric)
    log.info(f"Optimal score found at {k_opt} with {metric} score:  {quality_dict[k_opt][metric]:.2f}")

    return k_opt
