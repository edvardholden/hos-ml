# TODO want to support sampling of features, as well as solved and admissible labels.
import multiprocessing
import os
import typing
from enum import Enum
from typing import Dict, List, Tuple, Optional
import numpy as np
import pandas as pd
from xgboost import XGBClassifier
import logging

from hos.solver_features import (
    get_solver_state_features,
    fit_feature_processors,
    process_features,
)

log = logging.getLogger()

MINIMUM_RUNTIME_TARGET: float = 4  # Remove problems that are solved faster than this as they are too easy.

EMBEDDING_MODEL_PARAMETERS = {
    "HNN4": {
        "colsample_bytree": 0.8,
        "gamma": 0.05,
        "learning_rate": 0.05,
        "max_depth": 12,
        "min_child_weight": 5,
        "n_estimators": 50,
        "reg_alpha": 1.0,
        "reg_lambda": 1.0,
        "subsample": 0.8,
    }
}


class TargetType(str, Enum):
    # TODO set this from the input args!  # TODO looks like this is not provided??
    ADMISSIBLE = "admissible"
    SOLVED = "solved"


def get_solved_unsolved_indexes(df: pd.DataFrame) -> Tuple[List[str], List[str]]:
    solved = list(df.loc[df.notna().any(axis=1)].index)
    unsolved = list(df.loc[df.isna().all(axis=1)].index)
    return solved, unsolved


def filter_evaluation_features(df_eval: pd.DataFrame) -> pd.DataFrame:
    # Filter based on minimum evaluation runtime
    fastest_runtimes = df_eval.replace({np.nan: float("inf")}).min(axis=1, skipna=True)
    df_eval = df_eval.loc[(fastest_runtimes >= MINIMUM_RUNTIME_TARGET)]
    return df_eval


def align_df_indexes(df_feat: pd.DataFrame, df_target: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame]:
    # Clip dataframes to the problems in the intersection
    inter_ids = sorted(set(df_feat.index).intersection(set(df_target.index)))
    return df_feat.loc[inter_ids], df_target.loc[inter_ids]


def get_index_intersection(*dfs) -> List[str | int]:
    indexes = set(dfs[0].index)
    for df in dfs:
        indexes = indexes.intersection(df.index)
    return sorted(indexes)


def compute_admissible_evaluation_matrix(df: pd.DataFrame, adm_sc: float, adm_sf: float) -> pd.DataFrame:
    # FIXME. Redundant admissible definition due to circular import.
    # Remove unsolved problems
    df = df.dropna(axis="index", how="all")
    log.debug(f"Problems after removing unsolved: {len(df.index)}")

    # Get the fastest times
    fastest = df.min(axis=1)

    # Compute whether the heuristics are admissible
    df_adm = df.le(fastest + adm_sc, axis=0) | df.le(fastest * adm_sf, axis=0)
    return df_adm


def compute_feature_target(
    df: pd.DataFrame, target_type: TargetType, adm_sc: float, adm_sf: float
) -> pd.DataFrame:
    if target_type is TargetType.SOLVED:
        return df.notna().astype(int)
    elif target_type is TargetType.ADMISSIBLE:
        return compute_admissible_evaluation_matrix(df, adm_sc, adm_sf)  # TODO problematic redundancy
    else:
        raise ValueError(f"Target function for target type '{target_type}' is not implemented.")


def compute_unsolved_problem_embeddings(
    problem_collection: str,
    df_eval: pd.DataFrame,
    target_type: TargetType,
    adm_sc: float,
    adm_sf: float,
    file_path: Optional[str] = None,
) -> Dict[str, List[List[float]]]:
    # Load the solver state features
    df_feat = get_solver_state_features(problem_collection, file_path=file_path)

    df_eval = filter_evaluation_features(df_eval)  # It is safe to filter on solving times prior to split

    # Truncate the dfs according to the feature results
    indexes = get_index_intersection(df_eval, df_feat)
    df_eval = df_eval.loc[indexes]
    df_feat = df_feat.loc[indexes]

    # Get the training and testing ids via solved/unsolved evaluation
    train_ids, test_ids = get_solved_unsolved_indexes(df_eval)
    if len(train_ids) == 0:
        log.warning("Found no training ids for embedding model")
    if len(test_ids) == 0:
        log.warning("Found no testing ids for embedding model")

    df_target = compute_feature_target(df_eval.loc[train_ids], target_type, adm_sc, adm_sf)

    # Compute the targets for the training data
    df_train_feat, df_target = align_df_indexes(df_feat, df_target)

    # Perform model training
    fit_feature_processors(df_train_feat.values)
    x_train = process_features(df_train_feat.values)

    # Initialise and train the model
    model = TreeEmbedder(EMBEDDING_MODEL_PARAMETERS["HNN4"])
    model.fit(x_train, df_target)

    # Predict unsolved vectors
    x_test = process_features(df_feat.loc[test_ids].values)
    embeddings = model.predict(x_test)
    result = {prob: emb for prob, emb in zip(test_ids, embeddings)}
    # TODO check whether the values are OK for full iterations.

    # Return the result
    return result


class TreeEmbedder:
    """
    XGBoost model for learning the problem embeddings.
    Also known as HNN4 is some contexts.
    """

    def __init__(self, model_params: Optional[Dict[str, float]] = None):
        self.clf: Dict[int, typing.Any] = {}
        if model_params is None:
            model_params = {}
        self.model_params = model_params
        # self.prep_pipeline = {} # TODO later for saving the model
        self.training_features = None
        self.training_targets = None
        self.heuristics: List[int] = []

    def fit(self, X, y: pd.DataFrame):
        # Store the training data in the model
        self.heuristics = list(y.columns)  # type: ignore
        self.training_features = X
        self.training_targets = y[sorted(set(y.columns))]
        assert len(self.training_targets) == len(self.training_features)  # type: ignore

        # Train one model for each heuristic
        k_args = [(heur,) for heur in self.heuristics]
        pool = multiprocessing.Pool(processes=os.cpu_count() - 1)  # type: ignore
        res = pool.starmap(self._fit_target, k_args)
        pool.close()
        pool.join()

        # Store the result
        self.clf = {k: v for k, v in res}
        assert set(self.heuristics) == set(self.clf.keys())
        return self

    def _get_base_model(self):
        return XGBClassifier(
            n_jobs=1,
            **self.model_params,
            use_label_encoder=False,
            eval_metric="logloss",
        )

    def _get_naive_model(self):
        return StaticPredictor()

    def _fit_target(self, heur: int) -> Tuple[int, typing.Any]:
        # Initialise and train the model
        assert self.training_targets is not None
        assert self.training_features is not None  # type: ignore
        labels = self.training_targets[heur].astype(int).values

        if len(np.unique(labels)) < 2:
            log.warning(
                f"All label entries contained the same target for heuristic {heur}, using naive method"
            )
            model = self._get_naive_model()
        else:
            model = self._get_base_model()
        model.fit(self.training_features, labels)

        # Return the result
        return heur, model

    def predict_proba(self, X):
        embeddings = []
        for heur in self.heuristics:
            # Predict the results
            # _,  = self._predict_binary(heur, X)
            pred = self.clf[heur].predict(X)
            # pred = self.clf[heur].predict_proba(X)
            embeddings.append(pred)

        return np.asarray(embeddings).T

    def predict(self, X):
        # Predict probabilities
        predictions = self.predict_proba(X)

        # Choose the heuristic with the highest probability
        predictions = predictions.round()
        return predictions

    def __str__(self):
        return "HNN4"


class StaticPredictor:
    def __init__(self):
        self.static_value = None

    def fit(self, X, y):
        self.static_value = y[0]

    def predict(self, X):
        pred = [self.static_value] * len(X)
        return pred
