"""
Module containing the key cluster computation methods.
"""
from random import sample
from typing import List
import logging
from enum import Enum
import sys

import hos.database as db
from hos.admissible import get_admissible_clusters

log = logging.getLogger()

_CLUSTERS = List[List[int]]


class ClusterMethod(str, Enum):
    SAMPLE_RANDOM = "sample_random"
    SAMPLE_SPLIT = "sample_split"
    ADMISSIBLE = "admissible"


def get_problem_clusters(
    cluster_method: ClusterMethod,
    problem_collection: str,
    heuristics: List[int],
    no_cluster_samples: int,
    max_cluster_population: int,
    **adm_params,
) -> _CLUSTERS:
    log.info(f"Cluster method: {cluster_method}")

    if cluster_method is ClusterMethod.SAMPLE_SPLIT:
        problem_clusters = get_problem_id_sample_split(problem_collection, no_cluster_samples)
    elif cluster_method is ClusterMethod.SAMPLE_RANDOM:
        problem_clusters = get_problem_id_sample_random(
            problem_collection, no_cluster_samples, max_cluster_population
        )

    elif cluster_method is ClusterMethod.ADMISSIBLE:
        problem_clusters = get_admissible_clusters(  # type: ignore
            heuristics, problem_collection, **adm_params
        )

        # If no admissible cluster was computed, sample the problems instead
        if problem_clusters is None or len(problem_clusters) == 0:
            if problem_clusters is not None and len(problem_clusters) == 0:
                log.warning(
                    "There were enough admissible evaluation features, but still no problems were returned"
                )
            log.info("Admissible clustering failed, computing random samples instead of admissible clusters.")
            problem_clusters = get_problem_id_sample_random(
                problem_collection, no_cluster_samples, max_cluster_population
            )

    else:
        log.error(f"No clustering call implemented for ClusterMethod: {cluster_method}")  # type: ignore
        sys.exit(1)

    # Report number of clusters and the cluster sizes
    log.info("# Cluster Result")
    log.info(f"Number of clusters: {len(problem_clusters)}")
    log.info("Clusters sizes: " + ", ".join(str(len(c)) for c in problem_clusters))

    return problem_clusters


def get_problem_id_sample_split(problem_collection: str, no_cluster_samples: int) -> _CLUSTERS:
    """
    Partition the problems into roughly even sized n number of clusters.
    """
    problem_ids = db.general.get_problem_ids_in_collection(problem_collection)
    problem_clusters = list(problem_ids[i::no_cluster_samples] for i in range(no_cluster_samples))
    log.debug(f"Splitting ids from {problem_collection} into {no_cluster_samples}")
    return problem_clusters


def get_problem_id_sample_random(
    problem_collection: str, no_cluster_samples: int, max_cluster_size: int
) -> _CLUSTERS:
    """
    Randomly sample i problems into n clusters.
    """
    # Get the problem ids
    problem_ids = db.general.get_problem_ids_in_collection(problem_collection)
    max_cluster_size = min(max_cluster_size, len(problem_ids))

    # Randomly sample the problem ids
    problem_clusters = []
    for n in range(no_cluster_samples):
        problem_clusters += [sample(problem_ids, max_cluster_size)]

    log.debug(
        f"Splitting ids from {problem_collection} into {no_cluster_samples} with maximum size {max_cluster_size}"
    )
    return problem_clusters
