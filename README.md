# HOS-ML

Heuristic Optimisation over Heterogeneous (diverse) instances via dynamic re-clustering and admissible problem
embedding.

This repository contains HOS-ML Phase 1 for discovering a set of good heuristics over a set of diverse problems.
The heuristics can be compiled into competitive schedules using HOS-ML Phase
2: [SCPeduler](https://gitlab.com/edvardholden/scpeduler).

## Installation

This project expects python10

The module uses SMAC depends on `swig`, installed as:

```bash
sudo-apt get install swig
```

### Quick module install

HOS-ML can quickly be installed as a module by running:

```bash
git clone git@gitlab.com:edvardholden/hos-ml.git
cd hos-ml
pip3 install -r requirements.txt
pip3 install .
```

### For Developers / iProver Users

If running hos-ml on the iProver cluster, or if developing on this package,
it is recommended to install the package in a virtual environment.

```bash
```bash
git clone git@gitlab.com:edvardholden/hos-ml.git
cd hos-ml
pyenv local 3.10
make venv
source .venv/bin/activate
```

This installs the required packages in a virtual environment, and activates it.
Depending on your setup, this environment may have to be activated every time you enter
the directory.

The database credentials must be placed in a file named `db_cred.py` (not in git).

## Testing

The tests can be run using ```make test``` and the full code quality pipeline can be run using `tox`.

The test suite and quality verification setup uses `tox`.

# Method overview

HOS-ML is a three-phase system designed for discovering and organising effective heuristics for problem instances
in first-order logic. The vast parameter (heuristic) space of automated theorem provers is vast which makes the tunable
and very efficient. However, the sheer number of different configurations makes it impractical and infeasible to
discover good heuristics by hand. Hence, this requires modern hyper-parameter optimisation methods such as
Bayesian Optimisation (BO).
Nonetheless, BO expects uniform behaviour while problems are heterogeneous and no global optimal heuristic for all
problems exists. The solution is to partition the problems into similarly behaving problem clusters and optimise
over each cluster independently. This is at the core of HOS-ML, as shown in the figure below.

<img src=".HLP_HMP_Overview.png"  alt="HOS-ML" class="center" width="800" height="700">

The behavioural "types" of problems are derived from their behavioural patterns, discovered by interleaving heuristic
optimisation (with BO) and dynamic re-clustering of the problems.
The optimisation phase provides new heuristics while providing heuristic evaluation features.
These features are transformed into an admissible representation which reveals which problems perform relatively well
under the same set of heuristics.
This is computed via KMedoids clustering.
As the clustering improves the conditions for BO, we form a feedback loop which again provides improved
and more fine-grained evaluation features for the next iteration.

However, the clustering excludes unsolved problems as they have no informative admissible features.
These problems are integrated into the discovered clusters by computing embedding them into admissible evaluation
features via their solver state features (prover statistics computed for a single heuristic with a low-timelimit).

# Getting started

The project consists of the package `hos` and two driver scripts:

* `run_optimiser.py`: The entry point of HOS-ML. Performs the main optimisation loop with running SMAC, performing
  problem clustering * and global
  evaluation of local heuristics.
* `run_iprover_target.py`: The target function ran by SMAC. Transforms options into heuristics and attempts the
  heuristic on the given problem cluster via the cluster-scripts.

## Quick start

A run of HOS-ML can be started by running:

```
python3 run_optimisation.py [problem_collection] [parameter_space]
```

where `problem_collection` is the name of the problem collection used for optimisation in the database,
and `parameter_space` is a json file containing an encoding of the prover parameters used for optimisation.

For most intents and purposes, HOS-ML is run for a long duration on the cluster via nohup, i.e.:

```
nohup python3 -u run_optimisation.py {args} &
```

The '-u' flag ensures unbuffered streams, typically advantageous in this setting.
For more information on the output and logs, please see the "Outputs" section below.

Another key parameter is the optional `initial_heuristics` optional which provides a baseline and an initialisation of
`global_heuristics`. Supplying this heuristic via this option can be used to re-start runs at a given point,
or to reduce the computation load, as if this option is not set, a new global heuristic is computed over the default
options in the parameter space. It is the user's responsibility to ensure the initial heuristics are computed over
the exact same problem collection as a set for the optimisation.

Other parameters and their explanation are described by running: ```python3 run_optimiser.py --help```

## Outputs

The output of HOS-ML is stored in multiple output files for easy inspection.
The stdout contains any crashes on the main script as well as any error messages on issues with running the prover
or incorrect results. This is typically redirected to `nohup.out` when running the scripts via nohup.
Inspecting this file is essential for ensuring the correct operation of the prover along with any issues concerning the
mains script. Other information is stored in log files, found in `logs/{strftime}/`.

The logs are as follows:

* **summary.log**: Provides an overview of the key information and results of the optimisation.
  Look here for the new results and heuristics.
* **run_iprover_target.log** Contains key information of the run of each target function call.
  Inspect this file for any issues with the target function.
* **run_optimiser.log** Contains key information for the run of the main optimisation loop including results and
  clustering.
* **smac.log** Contains the output generated by running SMAC. Might provide valuable debugging information.

## Experiment Configuration

The global and local wall clock timelimits are set in `config.py`:

* Global WC: Time limit for computing evaluation features and evaluation the most promising heuristics.
* Local WC: Time limit for discovering heuristics in the target scripts.

The other experiment configurations relating to `cluster-scripts` are defined in `EXP_ENV`
in `src/hos/experiment_config.py`. These parameters correspond the values set in `experiment_config` when
using the cluster-scripts. This is because HOS-ML depends on cluster scripts.

## Problem Encodings

If the problems are TFF or LTB can set via the appropriate parameters in `config.py`.
For TFF, the arguments for the clausifier are modified.

Applying the prover to a specific problem encoding might make if necessary to change the `TEST_PROBLEM` constant
(also in config.py), which is used to evaluate whether a heuristic is valid.

## Adding a new option

Adding a new option for optimisation or amending a previous option can be slightly daunting as there are multiple steps
involved.
The central challenge is that a single parameter and its values must be registered with the prover in the database, be
defined in the json encoding fo the parameter space and occur in the SMAC to iProver and the db to SMAC translation.

Here is an overview of the steps required for adding/updating a prover options.
Depending on the current state, not all steps may be necessary.

1) Register prover with the new options (see cluster-scripts documentation).
2) Define the option and its range in the json encoding of the parameter space. Please add the option to the appropriate
   section, or create a new section. Basic options are added by adding a new entry in the jjson. More complex options
   like lists, nested lists and sup-indexes require meta-parameters, see the current implementations for a guidelines.
   Please name the file uniquely for good measure.
3) Add the option to the SMAC to iProver translation function, defined in `translate_to_iprover.py`. Locate the
   appropriate section (inst, prep, abstr ..) and add the option according to the required format. Basic options are at
   the top, while lists and nested lists are defined towards the end of the section functions.
4) Add the option to the db to SMAC translation, defined in `src/hos/smac/parameter_space.py`. Nothing to do for basic
   options, while complex options must be registered in the appropriate list constants (at the top of the module file).

A common issue is to introduce a typo which makes the SMAC to iProver translation fail, or forgetting to update the
prover (making the experiments fail).

## Troubleshooting

When setting up HOS-ML for a new configuration, there are unfortunately many things to keep in mind to avoid errors.
Here are some entry points for starting to investigate common pitfalls:

* _No problems ran in the experiment_: The values in EXP_ENV are not set correctly. Good idea to test first with
  cluster-scripts in an `experiment_config` file.

* _Experiments are not running_: The machine groups defined in '.cluster_scripts' and config:machines do not align.

* _Experiments contain all errors_: The TEST_PROBLEM is inappropriate for the problem type: the clausifier path is
  wrong;
  TFF/not-TFF not set correctly; incorrect values in EXP_ENV.

* _Target function returns error as param dict is not empty_: There is a mismatch between the parameters in the json
  encoding and the translation of configurations to iProver parameters. Likely forgot to add the option
  in `src/hos/target/translate_to_iprover.py` or there is a small typo in some parameter name.

* _Could not connect to the database_: Missing `db_cred.py` file in the project directory.

* _Missing tables or columns_: The database was not initialised correctly. Look in `misc/sql_scripts/` for the correct
  table structures.

# BibTeX Citation

If you use HOS-ML, please cite:

```
@inproceedings{DBLP:conf/mkm/HoldenK21,
  author       = {Edvard K. Holden and
                  Konstantin Korovin},
  editor       = {Fairouz Kamareddine and
                  Claudio Sacerdoti Coen},
  title        = {Heterogeneous Heuristic Optimisation and Scheduling for First-Order
                  Theorem Proving},
  booktitle    = {Intelligent Computer Mathematics - 14th International Conference,
                  {CICM} 2021, Timisoara, Romania, July 26-31, 2021, Proceedings},
  series       = {Lecture Notes in Computer Science},
  volume       = {12833},
  pages        = {107--123},
  publisher    = {Springer},
  year         = {2021},
  url          = {https://doi.org/10.1007/978-3-030-81097-9\_8},
  doi          = {10.1007/978-3-030-81097-9\_8},
  timestamp    = {Thu, 29 Jul 2021 13:42:12 +0200},
  biburl       = {https://dblp.org/rec/conf/mkm/HoldenK21.bib},
  bibsource    = {dblp computer science bibliography, https://dblp.org}
}
```

## Authors

* **Edvard Holden**
* **Konstantin Korovin**

See also the list of [contributors](https://gitlab.com/edvardholden/hos-ml/-/graphs/master) who participated in this
project.

