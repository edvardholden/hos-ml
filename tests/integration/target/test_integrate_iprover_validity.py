from hos.target.translate_to_iprover import get_heuristic_from_options
from hos.target.validity import check_heuristic_validity

PROVER_PATH = "tests/res/iproveropt_2022_02_23"


def test_check_heuristic_validity_invalid_options():
    heur = get_heuristic_from_options({"INVALID_OPTION": 12345})
    res = check_heuristic_validity(heur, prover_path=PROVER_PATH)
    assert res.is_valid is False
    assert res.returncode != 0
    assert res.cmd is not None and "INVALID_OPTION" in res.cmd
    assert res.cmd is not None and PROVER_PATH in res.cmd
    assert res.cmd is not None and "dbg_just_parse" in res.cmd


def test_check_heuristic_validity_empty_options():
    heur = get_heuristic_from_options({})
    res = check_heuristic_validity(heur, prover_path=PROVER_PATH)
    assert res.is_valid is True
    assert res.returncode == 0
    assert res.cmd is not None and PROVER_PATH in res.cmd
    assert res.cmd is not None and "dbg_just_parse" in res.cmd


def test_check_heuristic_validity_valid_subset():
    heur = get_heuristic_from_options(
        {
            "instantiation_flag": "true",
            "inst_lit_sel": "[+sign;+ground;-num_symb]",
            "inst_subs_new": "true",
        }
    )
    res = check_heuristic_validity(heur, prover_path=PROVER_PATH)
    assert res.is_valid is True
    assert res.returncode == 0
    assert res.cmd is not None and PROVER_PATH in res.cmd
    assert res.cmd is not None and "dbg_just_parse" in res.cmd
