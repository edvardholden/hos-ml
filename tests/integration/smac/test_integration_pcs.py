from mock import mock
from hos.smac.parameter_space import get_smac_configuration_space


@mock.patch("hos.smac.parameter_space.db")
def test_get_smac_configuration_space_no_exp_params(mock_db):
    mock_db.experiment.get_experiment_options.return_value = {}
    res = get_smac_configuration_space("tests/res/test_pcs.json", 12345, None)
    assert len(res) == 226


@mock.patch("hos.smac.parameter_space.db")
def test_get_smac_configuration_space_update_with_exp(mock_db):
    # Experiment vip:64741 - with slight modifications
    db_res = [
        ("--prolific_symb_bound", 256),
        ("--comb_res_mult", 3),
        ("--comb_inst_mult", 3),
        ("--comb_sup_mult", 8),
        ("--comb_sup_deep_mult", 2),
        ("--prop_solver_per_cl", 512),
        ("--subs_bck_mult", 8),
        ("--inst_solver_per_active", 1024),
        ("--inst_learning_start", 2048),
        ("--inst_learning_factor", 2),
        ("--inst_start_prop_sim_after_learn", 3),
        ("--inst_activity_threshold", 512),
        ("--sup_iter_deepening", 2),
        ("--sup_restarts_mult", 16),
        ("--sup_share_max_num_cl", 500),
        ("--sup_smt_interval", 512),
        ("--share_sel_clauses", "true"),
        ("--preprocessing_flag", "true"),
        ("--prep_sup_sim_sup", "false"),
        ("--prep_sup_sim_all", "true"),
        ("--instantiation_flag", "true"),
        ("--inst_dismatching", "true"),
        ("--inst_eager_unprocessed_to_passive", "true"),
        ("--inst_prop_sim_given", "true"),
        ("--inst_prop_sim_new", "false"),
        ("--inst_subs_new", "false"),
        ("--inst_eq_res_simp", "true"),
        ("--inst_subs_given", "true"),
        ("--inst_orphan_elimination", "true"),
        ("--inst_learning_loop_flag", "true"),
        ("--inst_restr_to_given", "true"),
        ("--inst_lit_activity_flag", "true"),
        ("--inst_sos_flag", "true"),
        ("--inst_sos_phase", "false"),
        ("--resolution_flag", "false"),
        ("--superposition_flag", "true"),
        ("--demod_use_ground", "true"),
        ("--sup_prop_simpl_new", "true"),
        ("--sup_prop_simpl_given", "true"),
        ("--sup_fun_splitting", "false"),
        ("--proof_out", "false"),
        ("--preprocessed_out", "false"),
        ("--time_out_real", "100"),
        ("--comb_mode", "clause_based"),
        ("--conj_cone_tolerance", "3"),
        ("--extra_neg_conj", "none"),
        ("--inst_lit_sel_side", "none"),
        ("--inst_solver_calls_frac", "1"),
        ("--inst_passive_queue_type", "queue"),
        ("--inst_sel_renew", "solver"),
        (
            "--inst_passive_queues",
            "[[+age;+num_lits;-age];[-age;-min_def_symb;+bc_imp_inh]]",
        ),
        ("--inst_passive_queues_freq", "[2;2]"),
        ("--inst_lit_sel", "[+split;-sign;-depth]"),
        ("--inst_sos_sth_lit_sel", "[+split;-ground;-depth]"),
        ("--sup_passive_queue_type", "priority_queues"),
        ("--demod_completeness_check", "fast"),
        ("--sup_to_prop_solver", "passive"),
        ("--sup_score", "sim_d_gen"),
        ("--sup_share_score_frac", "0.2"),
        ("--sup_symb_ordering", "invfreq"),
        (
            "--sup_passive_queues",
            "[[-conj_dist;-num_symb];[+age;-num_symb];[+score;-num_symb]]",
        ),
        ("--sup_passive_queues_freq", "[1;4;4]"),
        ("--sup_indices_passive", "[FwDemod;LightNorm]"),
        ("--sup_immed_triv", "[]"),
        ("--sup_input_triv", "[Unflattening;SMTSimplify]"),
        (
            "--sup_full_fw",
            "[SubsumptionRes;UnitSubsAndRes;FullSubsAndRes;LightNorm;ACNormalisation;GroundJoinability]",
        ),
        ("--sup_immed_fw_main", "[UnitSubsAndRes;LightNorm;ACNormalisation]"),
        ("--sup_immed_fw_immed", "[UnitSubsAndRes;ACNormalisation]"),
        (
            "--sup_input_fw",
            "[Subsumption;UnitSubsAndRes;FullSubsAndRes;Demod;LightNorm;ACNormalisation;ACDemod;GroundJoinability]",
        ),
        ("--sup_full_bw", "[Subsumption;SubsumptionRes;FullSubsAndRes;Demod]"),
        ("--sup_immed_bw_main", "[SubsumptionRes;Demod]"),
        ("--sup_immed_bw_immed", "[Subsumption;SubsumptionRes;UnitSubsAndRes]"),
        ("--sup_input_bw", "[Subsumption;SubsumptionRes;UnitSubsAndRes;Demod;ACDemod]"),
        ("--clausifier", "/shareddata/homes/holden/bin/vclausify_rel"),
        ("--clausifier_options", "--mode tclausify -t 100.00 "),
        ("--schedule", "none"),
        ("--out_options", "none"),
        ("--stats_out", "none"),
    ]

    mock_db.experiment.get_experiment_options.return_value = db_res
    res = get_smac_configuration_space("tests/res/test_pcs.json", 12345, None)
    assert len(res) == 226
