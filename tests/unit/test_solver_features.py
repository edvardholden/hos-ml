import numpy as np
import pandas as pd
from mock import mock

from hos.solver_features import (
    remove_problems_with_incomplete_features,
    filter_problem_entries,
    get_solver_state_features,
    filter_static_features,
    convert_output_dicts_to_df,
    extract_stats_from_output,
)


def test_remove_problems_with_incomplete_features():
    # Should remove features with all zero, static and with nan
    data = np.asarray(
        [[0, 0, np.nan, 1.0, 2.0, 50, 123], [0, np.nan, np.nan, np.nan, 5.0, 0, 123]]
    )
    df = pd.DataFrame(data, columns=list(range(7)))
    res = remove_problems_with_incomplete_features(df)
    assert len(res.index) == 2
    assert len(res.columns) == 4
    assert list(res.columns) == [0, 4, 5, 6]


def test_filter_static_features():
    # Should remove features with all zero, static and with nan
    data = np.asarray(
        [[0, 0, np.nan, 1.0, 2.0, 50, 123], [0, np.nan, np.nan, np.nan, 5.0, 0, 123]]
    )
    df = pd.DataFrame(data, columns=list(range(7)))
    res = filter_static_features(df)
    assert len(res.index) == 2
    assert len(res.columns) == 5
    assert list(res.columns) == [1, 2, 3, 4, 5]


@mock.patch("hos.solver_features.MAX_PARSING_TIME", 2)
@mock.patch(
    "hos.solver_features.REMOVE_PROBLEMS_SOLVED_DURING_FEATURE_EXTRACTION", True
)
@mock.patch("hos.solver_features.filter_problems_on_parsing_time")
@mock.patch("hos.solver_features.remove_problems_solved_during_extraction")
def test_filter_problem_entries_all_filters(mock_solved, mock_parse):
    mock_df = mock.MagicMock(pd.DataFrame)
    _ = filter_problem_entries(mock_df)
    mock_solved.assert_called_once()
    mock_parse.assert_called_once()


@mock.patch("hos.solver_features.MAX_PARSING_TIME", None)
@mock.patch(
    "hos.solver_features.REMOVE_PROBLEMS_SOLVED_DURING_FEATURE_EXTRACTION", False
)
@mock.patch("hos.solver_features.filter_problems_on_parsing_time")
@mock.patch("hos.solver_features.remove_problems_solved_during_extraction")
def test_filter_problem_entries_no_filters(mock_solved, mock_parse):
    mock_df = mock.MagicMock(pd.DataFrame)
    _ = filter_problem_entries(mock_df)
    mock_solved.assert_not_called()
    mock_parse.assert_not_called()


@mock.patch("hos.solver_features.os")
@mock.patch("hos.solver_features.get_prover_exec")
@mock.patch("hos.solver_features.load_features_as_dataframe")
@mock.patch("hos.solver_features.compute_and_load_features")
@mock.patch("hos.solver_features.filter_problem_entries")
def test_get_solver_state_features_exists(
    mock_filter, mock_compute, mock_load, mock_os, mock_prover
):
    mock_prover.return_value = "iproveropt"
    mock_os.path.exists.return_value = True
    get_solver_state_features("collection")
    mock_load.assert_called_once()
    mock_compute.assert_not_called()
    mock_filter.assert_called_once()


@mock.patch("hos.solver_features.get_prover_exec")
@mock.patch("hos.solver_features.os")
@mock.patch("hos.solver_features.load_features_as_dataframe")
@mock.patch("hos.solver_features.compute_and_load_features")
@mock.patch("hos.solver_features.filter_problem_entries")
def test_get_solver_state_features_compute(
    mock_filter, mock_compute, mock_load, mock_os, mock_prover
):
    mock_prover.return_value = "iproveropt"
    mock_os.path.exists.return_value = False
    get_solver_state_features("collection")
    mock_load.assert_not_called()
    mock_compute.assert_called_once()
    mock_filter.assert_called_once()


def test_convert_output_dicts_to_df():
    data = {
        "a.p": {"1": 1, "2": 2, "l": 3, "k": 4},
        "b.p": {"1": 4, "2": 5, "l": 6, "k": 7},
    }
    res = convert_output_dicts_to_df(data)  # type: ignore
    assert len(res.index) == 2
    assert len(res.columns) == 4


def test_extract_stats_from_output_empty():
    res = extract_stats_from_output("")
    assert res == {}


@mock.patch("hos.solver_features.PROVER_STATS_SECTIONS", ["Resolution"])
def test_extract_stats_from_output_single():
    out = (
        "------ Resolution\n"
        "\n"
        "res_num_of_clauses: 184\n"
        "res_num_in_passive: 157\n"
        "res_num_in_active: 27\n"
        "res_num_of_loops: 123\n"
        "res_undefined: undef\n"
        "\n"
        "------ Superposition\n"
        "\n"
        "sup_num_of_clauses: 347\n"
        "sup_num_in_active: 62\n"
        "sup_num_in_passive: 285\n"
    )

    res = extract_stats_from_output(out)
    assert res == {
        "res_num_of_clauses": 184.0,
        "res_num_in_passive": 157.0,
        "res_num_in_active": 27.0,
        "res_num_of_loops": 123.0,
        "res_undefined": 0.0,
    }
