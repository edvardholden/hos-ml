import pytest

from hos.input_parser import get_optimisation_input_args


@pytest.mark.parametrize("inp", [[], ["collection"], ["pcs"]])
def test_compute_missing_args(inp):
    with pytest.raises(SystemExit):
        get_optimisation_input_args(args=inp)


def test_compute_correct_args():
    inp = ["collection", "pcs"]
    args = get_optimisation_input_args(args=inp)
    assert args.problem_collection == "collection"
    assert args.parameter_space == "pcs"
