import pandas as pd
from mock import mock

from hos.stats import (
    compute_admissible_problem_dict,
    compute_set_universe,
    compute_top_heuristic_for_collection,
    compute_time_greedy_set_cover,
)


def test_compute_set_universe_empty():
    inp = {}  # type: ignore
    exp = set()
    res = compute_set_universe(inp)
    assert res == exp


def test_compute_set_universe_single():
    inp = {1: list(range(4))}  # type: ignore
    exp = set(range(4))
    res = compute_set_universe(inp)
    assert res == exp


def test_compute_set_universe_multiple():
    inp = {1: list(range(4)), 2: list(range(4, 10))}  # type: ignore
    exp = set(range(10))
    res = compute_set_universe(inp)
    assert res == exp


def test_compute_admissible_problem_dict():
    df = pd.DataFrame(
        [[1, 1, 0], [1, 0, 0], [1, 1, 0], [1, 0, 0]],
        index=[1, 2, 3, 4],
        columns=["h1", "h2", "h3"],
    )
    exp = {"h1": {1, 2, 3, 4}, "h2": {1, 3}, "h3": set()}
    res = compute_admissible_problem_dict(df)
    assert res == exp


@mock.patch("hos.stats.db")
def test_compute_top_heuristic_for_collection(mock_db):
    db_res = [(10, 102, 340), (11, 102, 300), (12, 94, 100)]
    mock_db.stats.query_heuristic_summary_solved_in_collection.side_effect = db_res

    best_heuristic, solved = compute_top_heuristic_for_collection(
        "collection", [10, 11, 12], 20.0
    )
    assert best_heuristic == 11
    assert solved == 102


def test_compute_time_greedy_set_cover_no_conflicting_heuristics():
    universe = set(range(20))
    subsets = {
        1: set(range(4)),
        2: set(range(4, 10)),
        3: set(range(3, 6)),
        4: set(range(9, 20)),
    }
    exp = [4, 2, 1]
    res = compute_time_greedy_set_cover(universe, subsets)
    assert res == exp
