from mock import mock, mock_open

from hos.target.experiment import ResultStatus
from hos.target.heuristic import Heuristic
from hos.target.validity import (
    check_heuristic_validity,
    init_invalid_heuristic_counter,
    check_reached_no_invalid_heuristics,
    log_invalid_heuristic,
    HeuristicValidityCheck,
    increment_invalid_heuristic_counter,
    handle_invalid_heuristic,
)


@mock.patch("hos.target.validity.check_if_heuristic_parses")
def test_check_heuristic_validity_no_solver(mock_parse):
    heur = Heuristic()
    heur.add_option("instantiation_flag", "false")
    heur.add_option("resolution_flag", "false")
    heur.add_option("superposition_flag", "false")

    res = check_heuristic_validity(heur)
    assert res.is_valid is False
    assert res.returncode == 1
    assert res.msg is not None and "No solver" in res.msg
    mock_parse.assert_not_called()


@mock.patch("hos.target.validity.check_if_heuristic_parses")
def test_check_heuristic_validity_inst_solver(mock_parse):
    heur = Heuristic()
    heur.add_option("instantiation_flag", "true")
    heur.add_option("resolution_flag", "false")
    heur.add_option("superposition_flag", "false")

    mock_res = mock.Mock()
    mock_parse.return_value = mock_res

    res = check_heuristic_validity(heur)
    assert res is mock_res
    mock_parse.assert_called_once_with(heur, None)


@mock.patch("hos.target.validity.create_counter_file")
def test_init_invalid_heuristic_counter(mock_create):
    init_invalid_heuristic_counter()
    mock_create.assert_called_once()


@mock.patch("builtins.open")
def test_check_reached_no_invalid_heuristics_no_counter(mock_open):
    res = check_reached_no_invalid_heuristics(None)
    assert res is False
    mock_open.assert_not_called()


#    with mock.patch("builtins.open", mock.mock_open(read_data=prob)):


def test_check_reached_no_invalid_heuristics_not_reached():
    with mock.patch("builtins.open", mock.mock_open(read_data="1")):
        res = check_reached_no_invalid_heuristics(10)
    assert res is False


def test_check_reached_no_invalid_heuristics_reached():
    with mock.patch("builtins.open", mock.mock_open(read_data="11")):
        res = check_reached_no_invalid_heuristics(10)
    assert res is True


def test_log_invalid_heuristic():
    check = HeuristicValidityCheck(is_valid=False, msg="Test message", returncode=-87)
    heur_id = 305678

    m = mock_open()
    with mock.patch("builtins.open", m):
        log_invalid_heuristic(heur_id, check)
    handle = m()
    handle.write.assert_called_once_with(
        f"Exp: {heur_id} ExitCode: -87 ErrorMsg: Test message"
    )


def test_increment_invalid_heuristic_counter():
    m = mock_open(read_data="12")
    with mock.patch("builtins.open", m):
        increment_invalid_heuristic_counter()
    handle = m()
    handle.write.assert_called_once_with("13")


@mock.patch("hos.target.validity.log_invalid_heuristic")
@mock.patch("hos.target.validity.increment_invalid_heuristic_counter")
@mock.patch("hos.target.validity.run_mock_experiment")
def test_handle_invalid_heuristic(mock_run, mock_count, mock_log):
    exp_conf, problem_set, machines = "exp_conf", "collection", "machines"
    mock_check = mock.Mock(spec=HeuristicValidityCheck)
    mock_check.returncode = 1
    mock_check.msg = "test"
    mock_run.return_value = 30478

    # Assert ran with .ERROR
    res = handle_invalid_heuristic(exp_conf, problem_set, machines, mock_check)
    assert res == 30478
    mock_run.assert_called_once_with(
        exp_conf, problem_set, machines, ResultStatus.ERROR
    )
    mock_count.assert_called_once()
    mock_log.assert_called_once_with(30478, mock_check)
