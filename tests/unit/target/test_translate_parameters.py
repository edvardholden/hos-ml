from hos.target.heuristic import Heuristic
from hos.target.translate_parameters import (
    translate_param,
    translate_param_flag,
    extract_list_non_order_boolean_param,
    extract_list_parameters,
    extract_nested_list_parameters,
)


def test_translate_param():
    heur = Heuristic()
    opt, val = "inst_opt", "kbo"
    param_dict = {opt: val}
    translate_param(heur, param_dict, opt, val)
    assert heur.get_option_value(opt) == val
    assert len(param_dict) == 0


def test_translate_param_flag_with_subtags():
    heur = Heuristic()

    param_dict = {
        "sup_input_fw_flag": "false",
        "sup_input_fw_Subsumption": "false",
        "sup_input_fw_SubsumptionRes": "true",
        "sup_input_fw_UnitSubsAndRes": "false",
    }

    tags = ["Subsumption", "SubsumptionRes", "UnitSubsAndRes"]
    translate_param_flag(heur, param_dict, "sup_input_fw", "[]", subtags=tags)

    assert heur.get_option_value("sup_input_fw") == "[]"
    print(param_dict)
    assert len(param_dict) == 0
    assert heur.get_no_options() == 1


def test_translate_param_flag_without_subtags():
    heur = Heuristic()
    param_dict = {
        "sup_input_fw_flag": "false",
        "sup_input_fw_Subsumption": "false",
        "sup_input_fw_SubsumptionRes": "true",
        "sup_input_fw_UnitSubsAndRes": "false",
    }

    translate_param_flag(heur, param_dict, "sup_input_fw", "[]")

    assert heur.get_option_value("sup_input_fw") == "[]"
    assert len(param_dict) == 3
    assert heur.get_no_options() == 1


def test_extract_list_non_order_boolean_param():
    heur = Heuristic()
    param_dict = {
        "sup_input_fw_flag": "true",
        "sup_input_fw_Subsumption": "false",
        "sup_input_fw_SubsumptionRes": "true",
        "sup_input_fw_UnitSubsAndRes": "true",
    }
    tags = ["Subsumption", "SubsumptionRes", "UnitSubsAndRes"]
    extract_list_non_order_boolean_param(heur, param_dict, "sup_input_fw", tags)

    assert heur.get_option_value("sup_input_fw") == "[SubsumptionRes;UnitSubsAndRes]"
    assert len(param_dict) == 0
    assert heur.get_no_options() == 1


def test_extract_list_parameters():
    heur = Heuristic()
    param_dict = {
        "inst_lit_sel_len": "3",
        "inst_lit_sel_metric_1": "-prop",
        "inst_lit_sel_metric_2": "+ground",
        "inst_lit_sel_metric_3": "+sign",
    }
    extract_list_parameters(heur, param_dict, "inst_lit_sel")
    assert heur.get_option_value("inst_lit_sel") == "[-prop;+ground;+sign]"
    assert len(param_dict) == 0
    assert heur.get_no_options() == 1


def test_extract_nested_list_parameters():
    heur = Heuristic()
    param_dict = {
        "inst_passive_queues_len": "2",
        "inst_passive_queues_1_len": "1",
        "inst_passive_queues_2_len": "2",
        "inst_passive_queues_1_freq": "3",
        "inst_passive_queues_2_freq": "4",
        "inst_passive_queues_metric_1_1": "+age",
        "inst_passive_queues_metric_2_1": "-ground",
        "inst_passive_queues_metric_2_2": "+conj",
    }
    extract_nested_list_parameters(heur, param_dict, "inst_passive_queues")
    assert heur.get_no_options() == 2
    assert heur.get_option_value("inst_passive_queues") == "[[+age];[-ground;+conj]]"
    assert heur.get_option_value("inst_passive_queues_freq") == "[3;4]"
    assert len(param_dict) == 0
