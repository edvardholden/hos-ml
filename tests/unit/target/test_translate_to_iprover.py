import pytest
from mock import mock

from hos.target.heuristic import Heuristic
from hos.target.translate_to_iprover import (
    add_static_experiment_options,
    check_if_param_dict_is_empty,
    add_clausifier_params,
)


@mock.patch.dict(
    "hos.target.translate_to_iprover.STATIC_EXPERIMENT_OPTIONS",
    {"res_param": "true", "inst_param": "2"},
    clear=True,
)
def test_add_static_experiment_options():
    heur = Heuristic()
    add_static_experiment_options(heur)
    assert heur.get_no_options() == 2
    assert heur.get_option_value("res_param") == "true"
    assert heur.get_option_value("inst_param") == "2"


def test_check_if_param_dict_is_empty_true():
    param_dict = {}  # type: ignore
    check_if_param_dict_is_empty(param_dict)


def test_check_if_param_dict_is_empty_false():
    param_dict = {"left": "over"}  # type: ignore
    with pytest.raises(ValueError):
        check_if_param_dict_is_empty(param_dict)


@mock.patch("hos.target.translate_to_iprover.PROBLEMS_IN_TFF", False)
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_OPTIONS", "extra_options")
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_PATH", "clausifier_path")
def test_add_clausifier_params():
    heur = Heuristic()
    param_dict = {}  # type: ignore
    add_clausifier_params(heur, param_dict)
    assert heur.get_no_options() == 2
    assert heur.get_option_value("clausifier") == "clausifier_path"
    assert (
        heur.get_option_value("clausifier_options")
        == "--mode clausify -t 300.00 extra_options"
    )


@mock.patch("hos.target.translate_to_iprover.PROBLEMS_IN_TFF", True)
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_OPTIONS", "")
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_PATH", "clausifier_path")
def test_add_clausifier_params_tff():
    heur = Heuristic()
    param_dict = {}  # type: ignore
    add_clausifier_params(heur, param_dict)
    assert heur.get_no_options() == 2
    assert heur.get_option_value("clausifier") == "clausifier_path"
    assert heur.get_option_value("clausifier_options") == "--mode tclausify -t 300.00"


@mock.patch("hos.target.translate_to_iprover.PROBLEMS_IN_TFF", False)
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_OPTIONS", "")
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_PATH", "clausifier_path")
def test_add_clausifier_params_sine():
    heur = Heuristic()
    param_dict = {
        "clausifier_sine_flag": "true",
        "clausifier_sine_sd": 3,
        "clausifier_sine_st": 1.2,
    }
    add_clausifier_params(heur, param_dict)
    assert heur.get_no_options() == 2
    assert heur.get_option_value("clausifier") == "clausifier_path"
    assert (
        heur.get_option_value("clausifier_options")
        == "--mode clausify -t 300.00 -ss axioms -sd 3 -st 1.2"
    )
    assert len(param_dict) == 0
