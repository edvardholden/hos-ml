from admissible_mapping.predict import predict_admissible_features
import sys
import numpy as np
import random
from sklearn.metrics.pairwise import pairwise_distances_argmin
from collections import defaultdict
import logging
import database
import math
from collections import Counter

from configX.cluster import (
    NO_CLUSTER_SAMPLES,
    MAX_PROBLEM_SAMPLE_SIZE,
    MIN_ADMISSIBLE_HEURISTIC,
    ADMISSIBLE_FILTER_RATIO,
    ADMISSIBLE_CLUSTERS_SUPPLEMENT_UNSOLVED,
    ADMISSSIBE_UNSOLVED_PREDICTION,
    SAMPLE_UNSOLVED_RATIO,
    ADMISSIBLE_PREDICTION_SAMPLING,
    N_CLOSEST_CLUSTERS,
    MIN_ADMISSIBLE_CLUSTER_POPULATION,
)
from configX.adm_prediction import FEATURE_DATA_FILE
from global_config import PROBLEM_COLLECTION
from config import dpath

import db_cred  # not in git

db_connection_details = db_cred.db_connection_details


# Configure the cluster logger
def get_cluster_logger():
    # Set format and file
    """
    logger = logging.basicConfig(format='%(levelname)s:%(message)s',
                        filename=dpath + 'hlp/cluster.log',
                        level=logging.INFO)
    """

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # create a file handler
    handler = logging.FileHandler(dpath + "wrappers/logs/cluster.log")
    handler.setLevel(logging.INFO)

    # create a logging format
    formatter = logging.Formatter("%(levelname)s:%(message)s")
    handler.setFormatter(formatter)

    # add the file handler to the logger
    logger.addHandler(handler)

    # Return
    return logger


# Get the cluster logger
cluster_logger = get_cluster_logger()


def get_admissible_clusters(heuristics):
    # TODO currently pruning this function

    # TODO need to get a better voerview of these function!
    # Only implement two? nearest neighbour and prediction?
    # Addd it is:
    # Map problem to cluster
    # Or select N-ratio problems clostest to this cluster..

    # Take the admissible predictions and assign the problems to the
    # appropaite cluster based on the given method
    '''
    cluster_logger.info("Supplementing problems using: {0}".format(ADMISSIBLE_PREDICTION_SAMPLING))
    if ADMISSIBLE_PREDICTION_SAMPLING == "SINGLE_PREDICTION":
        # This methid assign each problem to the closest cluster based on the
        # distance from the cluster center.
        pred_labels = pairwise_distances_argmin(
            unsolved_admissible, Y=cluster_centers, metric=common_solved
        )
        cluster_logger.info("Predicted labels from distance matrix: {0}".format(Counter(pred_labels)))

        # Group the predictions into clusters
        clusters_unsolved = group_problems_by_label(unsolved_problems_id, pred_labels)
        what about d = {**c, **d} TODO

        # Merge the unsolved and solved cluster
        clusters = defaultdict(list)
        for d in (clusters_unsolved, clusters_solved):
            for key, value in d.items():
                for v in value:
                    clusters[key].append(v)  # TODO: this could be done better
    '''

    elif ADMISSIBLE_PREDICTION_SAMPLING == "DISTANCE_SAMPLING":
        # This method computes the distance from each unsolved problem to each cluster center,
        # the clusters then get supplemented by the N closest problems to the center.

        # Compute the distance between cluster centers and predicted adm vector
        pred = pairwise_distances(unsolved_admissible, Y=cluster_centers, metric=common_solved)

        # Compute the cluster ranking for each problem
        cluster_ranks = {}
        for cluster_number in range(len(cluster_centers)):
            # Combine the name and distance
            rank = np.asarray([unsolved_problems_id] + [pred[:, cluster_number]], dtype=np.int64).T
            # Compute the rank and extract the order of the problem names
            rank = np.asarray(sorted(rank, key=lambda x: x[1]), dtype=np.int64)[:, 0]
            # Save the rank
            cluster_ranks[cluster_number] = list(rank)

        # Add the distance ranked problems to the clusters
        clusters = supplement_ranked_problems(clusters_solved, cluster_ranks)

"""
    elif ADMISSIBLE_PREDICTION_SAMPLING == "MAP_N_CLOSEST_CUSTERS":
        # Method for assigning a problem to it's N closests clusters

        # Get cluster label from closest to furthest per problem
        problem_distances = np.argsort(
            pairwise_distances(unsolved_admissible, Y=cluster_centers, metric=common_solved)
        )

        # Get the N closest cluster labels
        problem_labels = problem_distances[:, : min(len(cluster_centers), N_CLOSEST_CLUSTERS)]

        # Can just assign in a for loop?
        # Add the problems to the closest clusters
        clusters = clusters_solved.copy()
        for problem_id, problem_label in zip(unsolved_problems_id, problem_labels):
            for label in problem_label:
                clusters[label] += [problem_id]


"""
    return clusters


def supplement_ranked_problems(clusters_solved, cluster_ranks):
    cluster_logger.info("Sample unsolved problems with a ratio of: {0}".format(SAMPLE_UNSOLVED_RATIO))

    # For each problem cluster add the size of the problem cluster * sample_unsolved_ratio
    # Also ensure that we at least fill the sample size
    for key in clusters_solved.keys():
        # Check that key also exist in the cluster rank otherwise we skip this index
        if key not in cluster_ranks:
            cluster_logger.error("! No problems predicted for key: {0}".format(key))
            continue

        # Compute number of problems to sample
        supplement_size = max(
            min(math.ceil(int(len(clusters_solved[key]) * SAMPLE_UNSOLVED_RATIO)), len(cluster_ranks[key])),
            MAX_PROBLEM_SAMPLE_SIZE,
        )

        # Sample and supplement the problem cluster
        cluster_logger.info("Computed supplement_size: {0}".format(supplement_size))
        clusters_solved[key] += cluster_ranks[key][:supplement_size]

    # Return the new clusters
    return clusters_solved


# Get feature matric for the heuristic evaluations.
# The unsolved/incorrect problems (depending on context)
# are represented as None

def test_heuristics():
    heuristics = sys.argv[1:]
    print(heuristics)

    problems = get_problem_ids_in_collection(PROBLEM_COLLECTION)
    # Get the problem features
    evaluation = get_heuristic_evaluations(heuristics)

    # Split into solved vs unsolved problems (and trim the evaluations)
    solved_problems, unsolved_problems, evaluation = filter_solved_problem_evaluation(problems, evaluation)

    print("")
    print("evaluation")
    print(np.asarray(evaluation))

    admissible_evaluation = compute_admissible_evaluation(evaluation)
    solved_problems, admissible_evaluation = filter_problems_admissible_by_ratio(
        solved_problems, admissible_evaluation
    )
    # print()
    print("admissible_evaluation")
    # print(np.asarray(admissible_evaluation))
    print("No of unsolved problems: {0}".format(len(unsolved_problems)))

    admissible_labels = compute_admissible_labels(admissible_evaluation)

    print("% " * 5)
    print("No of problems: ", len(admissible_labels))
    print("No of clusters: ", len(set(admissible_labels)))
    print()

    for label in sorted(set(admissible_labels)):
        print()
        print("# " * 10)
        print("* Label:   ", label, " *")
        print("* No prob: ", Counter(admissible_labels)[label])
        for y, d in zip(admissible_labels, admissible_evaluation):
            if y == label:
                print(d)


if __name__ == "__main__":
    # test_heuristics()

    from global_config import INITIAL_HEURISTICS

    cluster_test_set = [
        INITIAL_HEURISTICS[:4],
        INITIAL_HEURISTICS[:6],
        INITIAL_HEURISTICS[:8],
        INITIAL_HEURISTICS[:12],
        INITIAL_HEURISTICS[:24],
    ]

    """
    for test in cluster_test_set:
        print("# ", test)
        now = time.localtime()
        cluster_logger.info("\nNew cluster call: {0}".format(time.strftime('%Y-%m-%d_%H-%M-%S.log', now)))
        get_admissible_clusters(test)
    #"""
    get_admissible_clusters([287583, 296939, 300000, 292049])
    # get_admissible_clusters(INITIAL_HEURISTICS[:8])

    # pass
