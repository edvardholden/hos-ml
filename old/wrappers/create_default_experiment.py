import global_config
import config
import argparse

from generate_exp_heur_string_from_json import load_param_options_from_json
from experiment_config import get_global_timeout, get_global_experiment_config
from target_create_heuristic import create_experiment_heuristic
from iprover import run_iprover_experiment
import database


def main():

    # Get global timeout
    timeout = get_global_timeout()

    # Get the heuristic from json file
    params = load_param_options_from_json()
    heur = create_experiment_heuristic(params, timeout=timeout, ignore_empty_check=True)
    prover_options = heur.get_prover_options()

    # Create experiment config
    exp_config = get_global_experiment_config(prover_options).to_file()

    # Run experiment
    exp_id = run_iprover_experiment(exp_config, global_config.PROBLEM_COLLECTION, config.machines)

    # Get and report exp id
    print("Ran experiment with ID: ", exp_id)

    # Report number of solved problems
    print("Solves: ", len(database.query_id_solved_problems_in_exp(exp_id, timeout)))


if __name__ == "__main__":
    main()
