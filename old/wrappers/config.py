#!/usr/bin/python3
import os
import parser_scenario
import glob
from pathlib import Path


def remove_empty_strs_list(str_list):
    return list(filter(None, str_list))


dpath = str(Path(__file__).parent.parent.absolute()) + "/"  # FIXME

invalid_heuristic_counter = "logs/invalid_heuristic_counter.log"
invalid_heuristic_log = "logs/invalid_heuristic_log.log"
tmp_folder = f"{dpath}tmp_exp_config/"
if not os.path.exists(tmp_folder):
    os.makedirs(tmp_folder)

scenario_file = dpath + "smac/config/scenario.txt"

# TODO deprecated
# scenario = parser_scenario.parse_scenario(scenario_file)
# parameter_space_file = scenario['pcs-file']

# problem_collection='tptp_v6.3_hard_epr_nhorn_eq'
# problem_collection='test_small'

# Current smac version
SMAC_VERSION = "v2.10.03-master-778 (3ee628ef9bf2)"


# Find the new scenario file
def replace_param_file_with_subset(this_smac_exp):
    # Read the file
    scenario_file_dict = parser_scenario.parse_scenario(scenario_file)

    # Load all files in the correct folder
    new_param_file = glob.glob(
        dpath + "smac/subset/" + str(this_smac_exp) + "/" + "optimisation_options.pcs"
    )[0]

    # Set new pcs filepath in scenario
    scenario_file_dict["pcs-file"] = new_param_file

    # Write new scenario to the subset folder for the experiment
    new_scenario_path = dpath + "smac/subset/" + str(this_smac_exp) + "/" + "scenario.txt"
    with open(new_scenario_path, "w") as f:
        for opt in scenario_file_dict:
            f.write(opt + " = " + scenario_file_dict[opt] + "\n")

    # Return scenario filepath
    return new_scenario_path


scenario_file_param = ["--scenario-file", scenario_file]

other_param = [
    "--always-run-initial-config",
    "true",
    "--initial-challengers",
    "DEFAULT",
    "--log-level",
    "TRACE",
    "--config-tracking",
    "true",
    "--console-log-level",
    "TRACE",
    "--save-runs-every-iteration",
    "true",
]

smac_params = other_param
