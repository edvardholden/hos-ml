import mysql.connector as db
import global_config
import os
import traceback
import sys
import config
import db_cred  # not in git
import numpy as np
import database
import time

dpath = config.dpath
db_connection_details = db_cred.db_connection_details

import logging
from config import dpath

# Configure the logger
def __get_warmstart_logger():

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # create a file handler
    handler = logging.FileHandler(dpath + 'wrappers/logs/warmstart.log')
    handler.setLevel(logging.INFO)

    # create a logging format
    formatter = logging.Formatter('%(levelname)s:%(message)s')
    handler.setFormatter(formatter)

    # add the file handler to the logger
    logger.addHandler(handler)

    # Return
    return logger


logger = __get_warmstart_logger()


def get_warmstart_heuristics(global_collection, local_collection, prover_options, prover_id, current_timeout, global_heuristics, incumbent):

    if global_config.WARMSTART_LIMIT_CURRENT_HLP_EXPERIMENT:
        logger.info("Using only current HLP experiment")

        # Experiment is limited to the current heuristics
        heur_smac = get_current_smac_experiments(prover_id, current_timeout)

        # The global heuristics are limited to the current experiment
        heur_global = get_experiments_with_paramstring(global_heuristics)


    else:
        logger.info("Using all appropriate experiments")

        # Get all experiments SMAC has run on these options and prover
        heur_smac = get_smac_experiments(global_collection, prover_options, prover_id, current_timeout)

        # Get the heuristics ran in all global evaluations
        heur_global = get_warmstart_heuristics_global_evaluation(
            global_collection, prover_options, prover_id, current_timeout)


    # Get the experiments which concerns the current local problem collection
    heur_smac_filtered = get_experiments_matching_ids_and_problem_set(local_collection, heur_smac)

    # Get the experiments which hace incorrect heuristics (the problem collection it doesn't matter)
    heur_illegal_options = get_experiments_with_illegal_heuristics(heur_smac)

    # Combine the appropriate heuristics
    heur_warm = sorted(set(heur_global + heur_smac_filtered + heur_illegal_options))

    logger.info("Initial SMAC heuristics: {0}".format(len(heur_smac)))
    logger.info("Appropriate for the current problem collection: {0}".format(len(heur_smac_filtered)))
    logger.info("Invalid heuristic information: {0}".format(len(heur_illegal_options)))
    logger.info("Number of global heuristics: {0}".format(len(heur_global)))
    logger.info("Resulting number of heuristics: {0}".format(len(heur_warm)))

    return heur_warm


def handle_warmstart(global_collection, local_collection, prover_options,
                     prover_id, current_timeout, global_heuristics, incumbent):

    # Function for getting appropriate experiments and prover options form the database
    # to create warmstart files for SMAC to use

    # Create new log entry
    now = time.localtime()
    logger.info("\nNew call: {0}".format(time.strftime('%Y-%m-%d_%H-%M-%S.log', now)))

    # Get the heuristics approparite for warmstarting
    heur_warm = get_warmstart_heuristics(global_collection, local_collection, prover_options, prover_id, current_timeout, global_heuristics, incumbent)

    # Check if we have any heuristics to warmstart from
    if len(heur_warm) == 0:
        logger.info("No heuristics for warmstart, returning None")
        return None

    # Make sure the entries are and that there are no duplicates
    heur_warm = sorted(set(heur_warm))
    logger.info("Create warmstart with {0} heuristics".format(len(heur_warm)))

    # Get quality and param strings from the appriate experiments
    param_string = get_parameter_string(heur_warm)
    quality = get_quality_warmstart(local_collection, heur_warm, current_timeout, incumbent)

    # Create folder if not exists
    folder_path = dpath + "smac/output/experiments/warmstart/"
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    # Create the string parameter file
    create_parameter_file(folder_path, param_string)
    # Store the mapping between the paramstrings and the evaluation
    create_run_result_file(folder_path, quality)

    # Return folder name/path
    return folder_path


def get_experiments_with_illegal_heuristics(experiments):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        # Make sure the list is not empty
        if len(experiments) == 0:
            return []

        experiment_list = ', '.join([str(exp) for exp in experiments])
        sql_query = '''
                    SELECT DISTINCT Experiment
                    FROM ProblemRun
                    WHERE Experiment IN
                    (
                        {0}
                    )
                    AND SZS_Status = "Error"
                    AND StatusOutput = "Illegal Heuristic"
                    ;'''.format(experiment_list)

        # Execute and get result
        curs.execute(sql_query)
        res = curs.fetchall()
        res = [r[0] for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_experiments_with_paramstring(experiments):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        # Make sure the list is not empty
        if len(experiments) == 0:
            return []

        # Create query string
        experiment_list = ', '.join([str(exp) for exp in experiments])
        sql_query = '''
                    SELECT iProverExperimentID
                    FROM SMAC_ParamString
                    WHERE iProverExperimentID IN ({0})
                    ORDER BY iProverExperimentID
                    ;'''.format(experiment_list)

        # Execute and get result
        curs.execute(sql_query)
        res = curs.fetchall()

        # Returns empty array if no matches
        res = [int(r[0]) for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


# This function is somewhat redundant of iprover.py:compute_weighted_solved
def compute_weighted_solved_score(solved_incumbent, experiment, current_timeout, problem_collection):

    # Get the problems solved in this experiment
    solved_this = database.query_solved_problems_filter_problems_solved_runtime(experiment, current_timeout, problem_collection)

    # Compute the score and return
    score = len(solved_this) + global_config.optimisation_metric_weight * len(set(solved_this).difference(set(solved_incumbent)))
    return score


def get_quality_warmstart(problem_collection, heuristics, current_timeout, incumbent):

    quality = []

    metric = global_config.optimisation_metric

    if metric == "total_solved":
        for heur in heuristics:
            score = database.query_no_solved_problems_filter_problems_solved_runtime(
                heur, current_timeout, problem_collection)

            # Make sure the quality is negated
            quality += [-int(score)]

    elif metric == "total_time":
        for heur in heuristics:
            score = database.query_tot_time_filter_problems_runtime(
                heur, current_timeout, problem_collection)

            # Make sure the quality is POS
            quality += [int(score)]

    elif metric == "weighted_solved":
        if incumbent is None:
            raise ValueError("ERROR: incumbent value is not provided for optimisation metric weighted_solved")

        # Get the problems solved in the incumbent
        solved_incumbent = database.query_solved_problems_filter_problems_solved_runtime(incumbent, current_timeout, problem_collection)

        for heur in heuristics:
            score = compute_weighted_solved_score(solved_incumbent, heur, current_timeout, problem_collection)

            # Make sure the quality is negated
            quality += [-int(score)]

    else:
        print("Optimisation metric \"{0}\" not implemented for warmstart".format(metric), file=sys.stderr)
        sys.exit(1)

    return quality


def create_parameter_file(prefix, params):

    # Create param string
    param_string = ""
    for i, arg in enumerate(params):
        param_string += "{0}: {1}\n".format(i + 1, arg)

    # Write result to file
    with open(prefix + "paramstrings-it1.txt", 'w+') as pf:
        pf.write(param_string)


def create_run_result_file(prefix, quality):

    # The header of the resulting file
    run_header = ["Run Number", "Run History Configuration ID", "Instance ID", "Response Value (y)", "Censored?",
                  "Cutoff Time Used", "Seed", "Runtime", "Run Length", "Run Result Code", "Run Quality",
                  "SMAC Iteration", "SMAC Cumulative Runtime", "Run Result", "Additional Algorithm Run Data",
                  "Wall Clock Time"]

    # Get length
    data_length = len(quality)

    # Initialise data
    dummy_data = [1, 1, 1, 24.12, 0, 1.797, -1, 0.0, 0.0, 1, 24.12, 0, 0.0, "SAT", "", 300]
    initial_data = np.asarray([dummy_data] * data_length)

    # Lets generate the non-dummy values
    run_number = list(range(1, data_length + 1))

    # Set run number
    initial_data[:, 0] = run_number

    # Set "Response Value (y)"
    initial_data[:, 3] = quality

    # Set "Run Quality"
    initial_data[:, 10] = quality

    # Create csv string
    rar = ','.join(run_header) + '\n'
    for data in initial_data:
        rar += ','.join(data) + '\n'

    # Write result to file
    with open(prefix + 'runs_and_results-it1.csv', 'w+') as pf:
        pf.write(rar)


def get_parameter_string(heuristics):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        # If there are no heuristics
        if len(heuristics) == 0:
            return []

        # Create the query string
        heuristic_string = ', '.join([str(heur) for heur in heuristics])
        sql_query = '''
                    SELECT ParamString
                    FROM SMAC_ParamString
                    WHERE iProverExperimentID IN ({0})
                    ORDER BY iProverExperimentID
                    ;'''.format(heuristic_string)

        # Execute and get result
        curs.execute(sql_query)
        res = curs.fetchall()
        res = [r[0] for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_smac_experiments(problem_collection, prover_options, prover_id, current_timeout):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        sql_query = '''
        SELECT DISTINCT ExperimentID
        FROM Experiment
        WHERE Experiment.ExperimentID IN
        (
            SELECT WrapperRun.Experiment
            FROM WrapperRun
            WHERE WrapperRun.SMACExperiment IN
            (
                SELECT SMACExperimentID
                FROM SMAC_ParallelExperiment
                WHERE PSMACExperimentID IN
                (
                    SELECT PSMACExperimentID
                    FROM SMAC_ParallelExperiment
                    WHERE HLP_ExperimentID IN
                    (
                        SELECT ExperimentID
                        FROM HLP_Experiment
                        WHERE ProblemCollection = %s
                        AND ExperimentPCSFile = %s
                    )
                )
            )
            AND WrapperRun.Experiment IN
            (
                SELECT SMAC_ParamString.iProverExperimentID
                FROM SMAC_ParamString
            )
        )
        AND Experiment.Prover = %s
        AND Experiment.TimeoutWallclock >= %s
        ;
        '''
        curs.execute(sql_query, (problem_collection, prover_options, prover_id, current_timeout))

        res = curs.fetchall()
        res = [int(r[0]) for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_current_smac_experiments(prover_id, current_timeout):
    # Same as above, but limits the experiments to the current HLP experiment

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        sql_query = '''
        SELECT DISTINCT ExperimentID
        FROM Experiment
        WHERE Experiment.ExperimentID IN
        (
            SELECT WrapperRun.Experiment
            FROM WrapperRun
            WHERE WrapperRun.SMACExperiment IN
            (
                SELECT SMACExperimentID
                FROM SMAC_ParallelExperiment
                WHERE PSMACExperimentID IN
                (
                    SELECT PSMACExperimentID
                    FROM SMAC_ParallelExperiment
                    WHERE HLP_ExperimentID =
                    (
                        SELECT MAX(ExperimentID)
                        FROM HLP_Experiment
                    )
                )
            )
            AND WrapperRun.Experiment IN
            (
                SELECT SMAC_ParamString.iProverExperimentID
                FROM SMAC_ParamString
            )
        )
        AND Experiment.Prover = %s
        AND Experiment.TimeoutWallclock >= %s
        ;
        '''
        curs.execute(sql_query, (prover_id, current_timeout))

        res = curs.fetchall()
        res = [int(r[0]) for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()

def get_warmstart_heuristics_global_evaluation(
        problem_collection, prover_options, prover_id, current_timeout):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        curs.execute('''
        SELECT DISTINCT ExperimentID
        FROM Experiment
        WHERE Experiment.ExperimentID IN
        (
            SELECT iProverExperimentID
            FROM HLP_EvaluationRun
            WHERE HLP_EvaluationRun.HLP_ExperimentID IN
            (
                SELECT ExperimentID
                FROM HLP_Experiment
                WHERE ProblemCollection = %s
                AND ExperimentPCSFile = %s
            )
        )
        AND Experiment.ExperimentID IN
        (
            SELECT SMAC_ParamString.iProverExperimentID
            FROM SMAC_ParamString
        )
        AND Experiment.Prover = %s
        AND Experiment.TimeoutWallclock >= %s
        ;''', (problem_collection, prover_options, prover_id, current_timeout))

        res = curs.fetchall()
        res = [int(r[0]) for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_experiments_matching_ids_and_problem_set(problem_collection, experiments):

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        # List to keep the end number of heuristic
        exp_filtered = []

        # Get the problems in the current collection
        curs.execute('''
                     SELECT Problem
                     FROM CollectionProblems
                     WHERE Collection = (SELECT CollectionID
                                         FROM Collection
                                         WHERE CollectionName=%s)
                     ;''', (problem_collection,))
        res = curs.fetchall()
        collection_problems = set([r[0] for r in res])

        # For each heuristic
        for exp in experiments:
            # Get problems in experiment
            curs.execute('''
                         SELECT Problem
                         FROM ProblemRun
                         WHERE Experiment=%s
                         ;''', (exp,))
            res = curs.fetchall()
            experiment_problems = set([r[0] for r in res])

            # Check if we are using a subset of these problems
            if collection_problems.issubset(experiment_problems):
                # If a subset, keep the heuristic
                exp_filtered += [exp]

        return exp_filtered

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


if __name__ == "__main__":
    #res = get_warmstart_heuristics('tptp_v7.4_fof_4000_rand', 'tptp_v7.4_fof_4000_rand', 'param_opt/test_22_01_21_sup.json', 458, 20, [310261], 310261)
    #res = handle_warmstart('tptp_v7.4_fof_4000_rand', 'tptp_v7.4_fof_4000_rand', 'param_opt/test_22_01_21_sup.json', 458, 20, [310261], 310261)
    #print(res)


    solved_incumbent = database.query_solved_problems_filter_problems_solved_runtime(310260, 100, 'tptp_v7.4_fof_4000_rand')
    print(len(solved_incumbent))
    score = compute_weighted_solved_score(solved_incumbent, 316470, 100, 'tptp_v7.4_fof_4000_rand')
    print(score)
    score = compute_weighted_solved_score(solved_incumbent, 310260, 100, 'tptp_v7.4_fof_4000_rand')
    print(score)



    #get_quality_warmstart(problem_collection, heuristic, current_timeout)
    #res = get_quality_warmstart("tptp_v7.4_fof_4000_rand", [281963, 287217, 287218, 287219, 287220, 287221, 287222], 300, 281963)
    #print(res)
