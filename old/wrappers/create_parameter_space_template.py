# Script that creates a template for the parameter space based
# on the prover options registered with the prover in the database
# Some of the default values for the options are transferred
# from the best run in the db. To make the template compatible with
# the parameters for the queues, a Q specification file is provided.
# The output is a json file where many fields must still be filled
# in, as well as it is easy to modify.

import mysql.connector as db
import db_cred
import sys
import traceback
import json
import re
import argparse
import database

# TODO Read file for conditional parameters (specify them in JSON)??

db_connection_details = db_cred.db_connection_details

# Default values for program
DEFAULT_PROVER_ID = 254
DEFAULT_WRITE_DESTINATION = 'data.json'
DEFAULT_QUEUE_SPECIFICATION_FILE = 'smac_special_param_specifications.json'

# Int and Real have [] instead of {} in rnge
PCS_FORMAT_STR_RANGE = "%s %s {%s} [%s]"
PCS_FORMAT_STR_INTERVAL = "%s %s [%s] [%s]"

# json format for an option in the output file. The way py/json
# creates json objects it is more convenient to just print the text
JSON_OUTPUT_FORMAT = '''{
    "opt_name": "%s",
    "opt_type": "%s",
    "opt_domain": "%s",
    "default_val": "%s",
    "possible_condition": "%s"
}'''

SPECIAL_PARAM = ['--res_passive_queues', '--res_passive_queues_freq'\
                , '--inst_passive_queues' , '--inst_passive_queues_freq'\
                , '--inst_lit_sel']


# Object to store the values of a prover option and perform operations on them
class ProverOption:

    def __init__(self, opt_name, opt_type, opt_domain="", default_val="", possible_condition=""):
        self.opt_name = opt_name
        self.opt_type = opt_type
        self.opt_domain = opt_domain
        self.default_val = default_val
        self.possible_condition = possible_condition

        # Used in translation between db and smac types
        self.dict_types = {'Bool': 'categorical', 'Float': 'real',
                  'Int': 'integer', 'String': 'categorical'}

        self.set_range_bool()

    # Return type based on in if the type is set from the DB or not
    def get_type(self):
        val = self.dict_types.get(self.opt_type)
        if val == None:
            return self.opt_type
        else:
            return val

    # The range of boolean variables are known, hence they are automatically added
    def set_range_bool(self):
        if self.opt_type == 'Bool':
            self.opt_domain = "true, false"

    # Returns the object in the specified JSON format
    def to_json(self):
        return JSON_OUTPUT_FORMAT %(self.opt_name, self.get_type(), self.opt_domain\
                , self.default_val, self.possible_condition)


# Function to get the registered options for a given prover
def query_parameter_name_type(prover_id):
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Get option and option type from db
        curs.execute('''
               SELECT ParameterName, ParameterValueType
               FROM ProverParameters
               WHERE Prover = %s
               ;''', (prover_id,))

        return curs.fetchall()

    except (db.Error) as err:
        print(err)
    except (Exception) as err:
        print(err)
    finally:
        if curs: curs.close()
        if conn: conn.close()


# Create list of options from prover_parameter_query
def create_options_class_list(option_type_list):
    options_list = []
    for option in option_type_list:
        options_list += [ProverOption(option[0], option[1])]

    return options_list


# Get the optimal experiment
def get_optimal_experiment_id():
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        # Experiment id of which to take prover options from, get current best incumbent based
        # on Quality. Experiments are set to be greater than 120 as the measure was inverted
        curs.execute('''
                     SELECT Experiment
                     FROM Incumbent
                          INNER JOIN WrapperRun WHERE
                          Incumbent.Run = WrapperRun.WrapperRunID
                          AND SMACExperiment > 120
                          ORDER BY Quality DESC
                          LIMIT 1;
                     ''' )

        return curs.fetchall()[0][0]

    except (db.Error) as err:
        print(err)
    except (Exception) as err:
        print(err)
    finally:
        if curs: curs.close()
        if conn: conn.close()



# Map optimal default values to the prover options
# If the the option cannot be found, the process just continues
def map_incumbent_to_prover(prover_options, optimal_options):

    # Create dict of (option, value) from incumbent
    opt_map = dict([(p[0], p[1]) for p in optimal_options])

    # Check if the option exists, and map it
    for option in prover_options:
        if option.opt_name in opt_map:
            option.default_val = opt_map[option.opt_name]

    return [["standard", prover_options]]


def insert_default_value_res(data):

    res = []

    for opt in data["res_passive_queues"]:
         res  += [ProverOption('--' + opt["opt_name"], opt["opt_type"], opt["opt_domain"]\
                               , opt["default_val"], opt["possible_condition"])]
    return res


def insert_default_value_inst(data):

    res = []

    for opt in data["inst_passive_queues"]:
         res += [ProverOption('--' + opt["opt_name"], opt["opt_type"], opt["opt_domain"]\
                               , opt["default_val"], opt["possible_condition"])]
    return res


def insert_default_value_inst_lit(data):

    res = []

    for opt in data["inst_lit_sel"]:
         res += [ProverOption('--' + opt["opt_name"], opt["opt_type"], opt["opt_domain"]\
                               , opt["default_val"], opt["possible_condition"])]
    return res

# Fetch the queue specification file and map the optimal values to this one
def add_queue_values_from_file(standard_options, queue_specification_file):

    # DELETE the prover options that needs special description in SMAC
    i = 0
    while i < len(standard_options[0][1]):
        if standard_options[0][1][i].opt_name in SPECIAL_PARAM:
            del standard_options[0][1][i]
        else:
            i += 1

    # Load Q specifications
    with open(queue_specification_file) as f:
        data = json.load(f)

    # Create Q options and map them to the appropriate incumbent value
    special_param_res = insert_default_value_res(data)
    special_param_inst = insert_default_value_inst(data)
    special_param_lit = insert_default_value_inst_lit(data)

    return [["res_passive_queue", special_param_res],
            ["inst_passive_queue", special_param_inst],
            ["inst_lit_sel", special_param_lit]]


# Write list of option specifications to list in json format
def write_to_json_file(prover_options_list, out_file):

    # Open file
    with open(out_file, "w") as out:

        # Structure is multiple objects containing arrays, therefore write header
        out.write("{")

        # Write n-1 first objects and arrays
        for i in range(0, len(prover_options_list)-1):

            # Convert objects to string
            json_list = [opt.to_json() for opt in prover_options_list[i][1]]

            # Create object for this array section
            type_string = " \"%s\": [" % prover_options_list[i][0]
            out.write(type_string)

            # Write n-1 arrays
            for n in range(0, len(json_list)-1):
                        out.write(json_list[n] + ', ')

            # Write n array (comma does not follow)
            out.write(json_list[-1] + '],\n')

        # Write n object with arrays
        type_string = " \"%s\": [" % prover_options_list[-1][0]
        out.write(type_string)

        json_list = [opt.to_json() for opt in prover_options_list[-1][1]]
        for n in range(0, len(json_list)-1):
                    out.write(json_list[n] + ', ')

        # Write n object
        out.write(json_list[-1] + ']\n')

        # Write end of file
        out.write("}")


def main():

    # Define input parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--prover_id', help='id of prover which to create template for', type=int)
    parser.add_argument('--optimal_experiment_id', help='experiment of which to get default values from', type=str)
    parser.add_argument('--smac_special_param_specification', help='file for specifying the queue parameters', type=str)
    parser.add_argument('--write_destination', help='destination and filename of where to write template', type=str)


    # ** Handle input parameters
    args = parser.parse_args()

    if args.prover_id:
        prover_id = args.prover_id
    else:
        prover_id = DEFAULT_PROVER_ID

    if args.optimal_experiment_id:
        optimal_experiment_id = args.optimal_experiment_id
    else:
        optimal_experiment_id = get_optimal_experiment_id()

    if args.smac_special_param_specification:
        special_param_file = args.smac_special_param_specification
    else:
        special_param_file = DEFAULT_QUEUE_SPECIFICATION_FILE

    if args.write_destination:
        write_destination = args.write_destination
    else:
        write_destination = DEFAULT_WRITE_DESTINATION


    # Get options from DB
    prover_options_list_db = create_options_class_list(query_parameter_name_type(prover_id))

    # Get incumbent options
    optimal_options = database.get_experiment_options(optimal_experiment_id)

    # Map options from incumbent to the prover options
    prover_options_list_standard = map_incumbent_to_prover(prover_options_list_db, optimal_options)

    # Replace Q options with alternatives from file
    prover_options_list_special = add_queue_values_from_file(prover_options_list_standard, special_param_file)

    # Combine all options for generating json file
    prover_options_list = prover_options_list_standard + prover_options_list_special

    # Write JSON data of options to file
    write_to_json_file(prover_options_list, write_destination)


if __name__ == "__main__":
    main()

