#!/usr/local/bin/python3
# parse smac scenario file into a disctionary
import configparser

def add_section_header(properties_file, header_name):
# configparser.ConfigParser requires at least one section header in a properties file.
# Our properties file doesn't have one, so add a header to it on the fly.
    yield '[{}]\n'.format(header_name)
    for line in properties_file:
        yield line

def parse_scenario(scenario_file):    
    file = open(scenario_file)
    config = configparser.ConfigParser()
    config.read_file(add_section_header(file, 'dummy_section'), source=scenario_file)
    data=dict(config['dummy_section'])
    return data
