import config
import os
import json


def map_standard_values(json_pcs_file, new_options):
    # Create dict of (option, value) from incumbent
    opt_map = dict([(p[0], p[1]) for p in new_options])

    for param in json_pcs_file["standard"]:
        if param["opt_name"] in opt_map:
            param["default_val"] = opt_map[param["opt_name"]]

    return json_pcs_file


def write_updated_pcs_json(data, smac_exp_id):
    # Make directory
    dir_name = config.dpath + "smac/subset/" + str(smac_exp_id) + "/"
    os.makedirs(dir_name, exist_ok=True)

    file_path = dir_name + 'pcs_data.json'

    with open(file_path, 'w') as f:
        json.dump(data, f, indent=4, separators=(',', ': '))

    return file_path


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def freeze_value(json_pcs_file, section, n):
    # Now we can replace the the domain with the fixed default value.
    # For real and integer values we change the type to ordinal,
    # such that we can set the current value as the domain
    if json_pcs_file[section][n]['opt_type'] == 'real' or json_pcs_file[section][n]['opt_type'] == 'integer':
        json_pcs_file[section][n]['opt_type'] = 'ordinal'

    # Freeze parameter range to the default value
    json_pcs_file[section][n]['opt_domain'] = json_pcs_file[section][n]['default_val']

    # Return the modified JSON
    return json_pcs_file


def condition_true(conditions, frozen_params):
    # Quick cheat: asusme we only have "and" and no "or" in our parameters
    conditions = conditions.split("&&")

    for cond in conditions:
        c = cond.split()

        # Find operator [> | < | == | !=] and evaluate the statement
        try:
            if c[1] == ">":
                st = int(frozen_params[c[0]]) > int(c[2])
            elif c[1] == "<":
                st = int(frozen_params[c[0]]) < int(c[2])
            elif c[1] == "!=":
                st = frozen_params[c[0]] != c[2]
            elif c[1] == "==":
                st = frozen_params[c[0]] == c[2]
            else:
                raise Exception("condition_true: unknown operator: ", c[1])
        except KeyError:
            # We might have a key error due to a parameter not being specified.
            # Very likely it is not specified due to other constraints.
            # Hence we return false
            return False

        # Return false if the statement is false
        if not st:
            return False

    # All conjunctions are true
    # Hence the overall statement must be true
    return True


def freeze_parameters(pcs_json_file, smac_exp_id, warm_domain):
    # Function for freezing sections of the parameter space

    # Load json file
    with open(pcs_json_file, 'r') as f:
        json_pcs_file = json.load(f)

    # Keep track of which parameters are frozen
    frozen_params = {}

    # # Freeze parameters and possibly their domain
    # Get each possible section
    for section in json_pcs_file:
        # Check if the section is frozen
        if section not in warm_domain:
            # Go through each param in each section
            for n in range(0, len(json_pcs_file[section])):
                # Freeze the value
                json_pcs_file = freeze_value(json_pcs_file, section, n)

                # Save the frozen value
                frozen_params[json_pcs_file[section][n]['opt_name']
                [2:]] = json_pcs_file[section][n]['opt_domain']

    # Check whether some conditions needs to be removed as the new domains might violate some conditions
    for section in json_pcs_file:
        # Go through each param in each section
        for n in range(0, len(json_pcs_file[section])):
            # Check if condition is non-empty
            if json_pcs_file[section][n]['possible_condition'] != "":

                # Get conditions
                _, conditions = json_pcs_file[section][n]['possible_condition'].split(" | ")

                # String for building new condition
                condition_string_list = []

                # Iterate over each condition and rebuild with the ones that still hold
                for cond in conditions.split(" || "):
                    # If params used in the conditions are frozen, and they no longer hold, they are removed
                    # if set(cond.split()).intersection(set(frozen_params.keys())) and not
                    # condition_true(cond, frozen_params):
                    if set(cond.split()).intersection(set(frozen_params.keys())):
                        pass
                    else:
                        condition_string_list += [cond]

                # Add the final reconstructed conditions
                if len(condition_string_list) > 0:
                    json_pcs_file[section][n]['possible_condition'] = json_pcs_file[section][n]['opt_name'][2:] + \
                                                                      " | " + ' || '.join(condition_string_list)
                else:
                    json_pcs_file[section][n]['possible_condition'] = ""

    # Write file to the exp subset folder
    file_path = write_updated_pcs_json(json_pcs_file, smac_exp_id)

    # Return location of file
    return file_path


###########################################################
##########################################################


if __name__ == "__main__":
    freeze_parameters('param_opt/test_02_02_21_sup.json', 8068, ['sup', 'abstr'])
