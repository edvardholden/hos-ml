# This script rins SMAC in parallel
# The main motivation for parallelisation is to more effectively utilise
# the cluster for running the iProver experiments. Create a the appropriate clusters
# in .cluster_scripts and supply their names to the iprover_machine_groups list.
# It redirects any input paramater to each instance of SMAC.py

import sys
import config
import os
import subprocess
import mysql.connector as db
import db_cred
import time
import socket
import traceback
import signal
from timeit import default_timer as time_now
import database
import global_config

# Specify the groups than runs iProver for each SMAC instance
# this changes based on which machines are available, and
# how the problem might be best distributed
host = socket.gethostname()
if host == "vip.crypt.cs.man.ac.uk":  # FIXME could be moved elsewhere?
    iprover_machine_groups = ["vip-iprover-1", "vip-iprover-2", "vip-iprover-3", "vip-iprover-4"]
elif host == "puppet":
    iprover_machine_groups = [
        "vip-iprover-1",
        "vip-iprover-2",
        "vip-iprover-3",
        "vip-iprover-4",
        "vip-iprover-5",
    ]
else:
    sys.exit("Unknown hostname for assigning cluster groups in smac_parallel_wait")

# List of prime numbers which can be used as seeds
prime_seeds = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]

dpath = config.dpath
db_connection_details = db_cred.db_connection_details

# Commands for running smac with nohup
NOHUP_SMAC_EXEC_PRE = "python3 smac.py "

# SMAC.py option for shared models (parallellisation)
SHARED_MODEL_ARG = "--parallel_smac {0} --parallel_smac_seed {1} "

# The path to the files SMAC uses toc ommunicate between instances
MODEL_FILES = dpath + "smac/output/experiments/" + "live-rundata-*"

# The timelimit we set for running a smac instance.
SMAC_INSTANCE_TIME_LIMIT = 43200  # 12 hours


# ### Functions for requesting and aquiring machines for iProver runs
# these functions assumes that a smac instance will never fail and then be re-started

# Function for aquiring a set of machines for computation
def lock_available_machine_set():
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor(buffered=True)

        res = curs.execute(
            """
                         START TRANSACTION;

                         SELECT @this_pexp_id := MAX(PSMACExperimentID)
                                                FROM SMAC_ParallelExperiment;

                         SELECT @node := Machines
                         FROM SMAC_ParallelExperimentMachines
                         WHERE Status="available"
                         AND PSMACExperimentID = @this_pexp_id
                         LIMIT 1;

                         UPDATE SMAC_ParallelExperimentMachines
                         SET Status="busy"
                         WHERE Machines=@node
                         AND PSMACExperimentID = @this_pexp_id;

                         COMMIT;

                         SELECT @node;
                         """,
            multi=True,
        )
        node = ""
        for cur in res:
            if str(cur) == "MySQLCursorBuffered: SELECT @node":
                node = cur.fetchall()[0][0]

        conn.commit()

        return node

    except (db.Error) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


# Function for releasing a set of machines after computation
def release_set_of_machines(machines):
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        curs.execute(
            """
                     UPDATE SMAC_ParallelExperimentMachines
                     SET Status="available"
                     WHERE Machines=%s
                     AND PSMACExperimentID = (SELECT MAX(PSMACExperimentID)
                                                     FROM SMAC_ParallelExperiment);
                     """,
            (machines,),
        )
        conn.commit()

    except (db.Error) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


##############################################
# Functions for driving the parallel SMAC run


# Need to delete old model files as SMAC doesn't know which ones that are currently in use
def delete_previous_live_rundata():
    try:
        subprocess.call("rm -f %s" % (MODEL_FILES), shell=True)

    except (subprocess.CalledProcessError) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(traceback.format_exc())
        print(err)


# Function for creating the appropriate db instances for the parallel run
def setup_parallel_experiment_in_db(machines_list):
    now = time.localtime()
    start_time = time.strftime("%Y-%m-%d %H:%M:%S", now)

    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Initialise database table for the SMACExperiment
        this_smacexp_id = database.initialise_smac_experiment_db(
            config.SMAC_VERSION, config.scenario_file, global_config.IPROVER_PARAMETERS_JSON
        )

        # Get the current PSMACID
        curs.execute(
            """
                     SELECT MAX(PSMACExperimentID)
                     FROM SMAC_ParallelExperiment
                     ;"""
        )
        res = curs.fetchall()[0][0]
        if res is None:
            parallel_smac_id = 0
        else:
            parallel_smac_id = res + 1

        # Get the current PSMACID
        curs.execute(
            """
                     SELECT MAX(ExperimentID)
                     FROM HLP_Experiment
                     ;"""
        )
        hlp_id = curs.fetchall()[0][0]

        # Initialise parallel experiment in db
        curs.execute(
            """
                     INSERT INTO SMAC_ParallelExperiment
                     (PSMACExperimentID, SMACExperimentID, HLP_ExperimentID,  Clusters, StartTime)
                     VALUES (%s, %s, %s, %s, %s)
                     ;""",
            (parallel_smac_id, this_smacexp_id, hlp_id, ", ".join(iprover_machine_groups), start_time),
        )
        conn.commit()

        # Create tuples of the experiment id and the machines
        machines_tuple = list(zip([parallel_smac_id] * len(machines_list), machines_list))

        # Load all machines into the DB
        for machine in machines_tuple:
            curs.execute(
                """
                         INSERT INTO SMAC_ParallelExperimentMachines
                         (PSMACExperimentID, Machines, Status)
                         VALUES (%s, %s, 'available')
                         ;""",
                machine,
            )
        conn.commit()

        # Return id of this smac experiment
        return this_smacexp_id

    except (db.Error) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


# Function for starting a SMAC instance for each set of machines
def start_smac_instaces(this_smacexp_id):
    # Get systenm arguments form smac_hlp
    if len(sys.argv) > 1:
        sys_arg = " ".join(sys.argv[1:])
    else:
        sys_arg = ""
        print(sys_arg)

    # Array for storing the processes
    smac_processes = []

    # Start processes and add them to the process list
    for group_no, _ in enumerate(iprover_machine_groups):
        proc = subprocess.Popen(
            [
                NOHUP_SMAC_EXEC_PRE
                + SHARED_MODEL_ARG.format(
                    this_smacexp_id, prime_seeds[group_no]
                )  # Add the parallel exp id and the instance seed
                + sys_arg
            ],
            shell=True,
            stdin=None,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            preexec_fn=os.setsid,
        )  # close_fds=True

        # Add process to list
        smac_processes.append(proc)
        # Halt execution between each start as the log files
        # are named according to the current time
        time.sleep(2)

    # Set start time
    start_time = time_now()

    # Check if processes are finished
    while len(smac_processes) > 0 and time_now() - start_time < SMAC_INSTANCE_TIME_LIMIT:
        # Check if process is dead
        for i, proc in enumerate(smac_processes):
            # Check if process has finished
            if proc.poll() is not None:
                # check if an error occured
                out, err = proc.communicate(timeout=15)
                if err != b"" and err != "":
                    print("err: ", err)
                    print("args: ", proc.args)
                if out != b"" and out != "":
                    print("out: ", out)
                    print("args: ", proc.args)
                # Process has finished, lets remove it
                del smac_processes[i]
        # Sleep for a bit
        time.sleep(5)

    # Kill the processes that haven't terminated due to timeout
    if len(smac_processes) > 0:
        for proc in smac_processes:
            # If process is still active
            if proc.poll() is None:
                try:
                    # Get process group id and kill the group
                    pgrp = os.getpgid(proc.pid)
                    os.killpg(pgrp, signal.SIGTERM)

                except ProcessLookupError:
                    # Process terminated before we could terminate it. The cpu time and proc_dict update
                    # therefore does not occur. The terminated process should be handled by the the
                    # terminated process handler function
                    pass

    # Program has finished running


def report_end_time():
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        curs.execute(
            """
                     SELECT MAX(PSMACExperimentID)
                     FROM SMAC_ParallelExperiment
                     ;"""
        )

        psmac_exp_id = curs.fetchall()[0][0]

        curs.execute(
            """
                     UPDATE SMAC_ParallelExperiment
                     SET EndTime = NOW()
                     WHERE PSMACExperimentID = %s
                     ;""",
            (psmac_exp_id,),
        )

        conn.commit()

    except (db.Error) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


# Get the iprover experiments that were ran in
# this iteration of runnign SMAC in parallel
def get_parallel_iprover_experiments():
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Get all the experiments which ran in this iteration
        curs.execute(
            """
                     SELECT Experiment FROM WrapperRun
                     WHERE SMACExperiment IN
                     (
                        SELECT SMACExperimentID
                        FROM SMAC_ParallelExperiment
                        WHERE PSMACExperimentID= (SELECT MAX(PSMACExperimentID) FROM SMAC_ParallelExperiment)
                     )
                     ;"""
        )
        experiments = curs.fetchall()
        experiments = [r[0] for r in experiments]
        return experiments

    except (db.Error) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def delete_parallel_machine_entries():
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        curs.execute(
            """
                     DELETE FROM SMAC_ParallelExperimentMachines
                     ;"""
        )
        conn.commit()

    except (db.Error) as err:
        print(err)
        print(traceback.format_exc())
    except (Exception) as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def main():
    try:
        # Delete current run files in folder
        # Not deleting these will confuse the different SMAC instances
        delete_previous_live_rundata()

        # Setup the SMACExperiment in the db and report it as a parallel experiment
        # The same experiment id will be used on all SMAC instances in the parallel run
        this_smacexp_id = setup_parallel_experiment_in_db(iprover_machine_groups)

        # Start running every instance of SMAC
        start_smac_instaces(this_smacexp_id)

        # Report when PSMAC finished
        report_end_time()

        # Clean up database based on the machines for this expeirment
        delete_parallel_machine_entries()

        """ TODO remove these for now as we handle it in the level above
        # Get the iprover experiments that here run in this iteration
        experiment_ids = get_parallel_iprover_experiments()

        # Check for errors that have occured while running iProver in this iteration
        check_iprover_experiment_errors(experiment_ids)

        # Check for unsoundness of SZS status
        check_iprover_experiment_incorrect_result(experiment_ids)
        """

    except Exception as exc:
        print(exc)
        print(traceback.format_exc())


if __name__ == "__main__":
    main()
