-- Used for tor the aid of running SMAC in parallel
DROP TABLE IF EXISTS SMAC_ParallelExperiment;
DROP TABLE IF EXISTS SMAC_ParallelExperimentMachines;

-- General Overview of a PSMAC experiment
CREATE TABLE SMAC_ParallelExperiment (
    PSMACExperimentID    INT UNSIGNED NOT NULL, -- Cannot have composite key with auto_increment
    SMACExperimentID     INT UNSIGNED NOT NULL,
    HLP_ExperimentID     INT UNSIGNED NOT NULL,
    Clusters             VARCHAR(256) NOT NULL,
    StartTime            DATETIME,
    EndTime              DATETIME,
    PRIMARY KEY (PSMACExperimentID, SMACExperimentID),
    FOREIGN KEY (SMACExperimentID) REFERENCES SMACExperiment(SMACExperimentID),
    FOREIGN KEY (HLP_ExperimentID) REFERENCES HLP_Experiment(ExperimentID)
);

-- Experiment to keep track of which machine groups are not in use
CREATE TABLE SMAC_ParallelExperimentMachines (
    PSMACExperimentID   INT UNSIGNED NOT NULL,
    Machines            VARCHAR(256) NOT NULL,
    Status              ENUM('available', 'busy') NOT NULL,
    PRIMARY KEY (PSMACExperimentID, Machines),
    FOREIGN KEY (PSMACExperimentID) REFERENCES SMAC_ParallelExperiment(PSMACExperimentID)
);



--ALTER TABLE SMAC_ParallelExperiment
--ADD COLUMN HLP_ExperimentID INT UNSIGNED NOT NULL AFTER SMACExperimentID,
--ADD FOREIGN KEY (HLP_ExperimentID) REFERENCES HLP_Experiment(ExperimentID) ;