DROP TABLE IF EXISTS Incumbent;
DROP TABLE IF EXISTS WrapperRun;
DROP TABLE IF EXISTS SMACExperiment;


CREATE TABLE SMACExperiment (
    SMACExperimentID           INT UNSIGNED NOT NULL AUTO_INCREMENT,
    SMACVersion                VARCHAR(256) NOT NULL,
    SMACExperimentScenario     TEXT NOT NULL,
    SMACExperimentPCS          TEXT NOT NULL,
    SMACExperimentStartTime    DATETIME NOT NULL,
    SMACExperimentEndTime      DATETIME,
    SMACExperimentDescription  VARCHAR(256),
    CONSTRAINT pk_SMACExperiment
        PRIMARY KEY (SMACExperimentID)
);

CREATE TABLE WrapperRun (
    WrapperRunID     INT UNSIGNED NOT NULL AUTO_INCREMENT,
    Runtime          FLOAT UNSIGNED NOT NULL,
    Quality          FLOAT NOT NULL,
    SMACExperiment   INT UNSIGNED NOT NULL,
    Experiment       INT NOT NULL,
    PossibleProblem  BOOLEAN NOT NULL,
    CONSTRAINT pk_WrapperRun
        PRIMARY KEY (WrapperRunID),
    CONSTRAINT wrapperRun_fk_SMACExperiment
        FOREIGN KEY (SMACExperiment)
            REFERENCES SMACExperiment (SMACExperimentID),
    CONSTRAINT wrapperRun_fk_Experiment
        FOREIGN KEY (Experiment)
            REFERENCES Experiment (ExperimentID)
);

CREATE TABLE Incumbent (
    IncumbentID  INT UNSIGNED NOT NULL AUTO_INCREMENT,
    Run          INT UNSIGNED NOT NULL,
    CONSTRAINT pk_Incumbent
        PRIMARY KEY (IncumbentID),
    CONSTRAINT incumbent_fk_Run
        FOREIGN KEY (Run)
            REFERENCES WrapperRun (WrapperRunID)
);


CREATE TABLE SMAC_ParamString (
iProverExperimentID int(11) NOT NULL,
ParamString VARCHAR(10000),
ParamFreeze VARCHAR(256),
PRIMARY KEY (iProverExperimentID),
FOREIGN KEY (iProverExperimentID) REFERENCES Experiment(ExperimentID)
);

