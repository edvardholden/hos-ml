-- Tables used to document the HLP_SMAC loop
DROP TABLE IF EXISTS HLP_Run;
DROP TABLE IF EXISTS HLP_Experiment;
DROP TABLE IF EXISTS HLP_ProblemProperties;
DROP TABLE IF EXISTS HLP_EvaluationRun;
DROP TABLE IF EXISTS HLP_RunMeasurement;

CREATE TABLE HLP_Experiment (
    ExperimentID        INT UNSIGNED AUTO_INCREMENT,
    StartTime           DATETIME NOT NULL,
    EndTime             DATETIME,
    ProblemCollection   VARCHAR(256) NOT NULL,
    KClusters           INT UNSIGNED NOT NULL,
    InitialHeuristics   VARCHAR(256),
    ExperimentPCSFile   VARCHAR(256),
    PRIMARY KEY (ExperimentID)
);


CREATE TABLE HLP_Run (
    HLPRunID        INT UNSIGNED NOT NULL AUTO_INCREMENT,
    ExperimentID    INT UNSIGNED NOT NULL,
    BestHeuristics  VARCHAR(256) NOT NULL,
    NewHeuristics   VARCHAR(256),
    Time            DateTime,
    Primary KEY (HLPRunID),
    FOREIGN KEY (ExperimentID) REFERENCES HLP_Experiment(ExperimentID)
);


CREATE TABLE HLP_ProblemProperties (
    CollectionID    INT(11) NOT NULL,
    ProblemID       INT(11) NOT NULL,
    Clauses         INT(11) NOT NULL,
    Conjectures     INT(11) NOT NULL,
    Epr             INT(11) NOT NULL,
    Horn            INT(11) NOT NULL,
    Unary           INT(11) NOT NULL,
    BinaryF         INT(11) NOT NULL,
    Lits            INT(11) NOT NULL,
    LitsEq          INT(11) NOT NULL,
    PRIMARY KEY (CollectionID, ProblemID),
    FOREIGN KEY (CollectionID) REFERENCES Collection(CollectionID)
);

CREATE TABLE HLP_EvaluationRun (
    HLP_ExperimentID     INT UNSIGNED NOT NULL,
    iProverExperimentID  int(11) NOT NULL,
    PRIMARY KEY (HLP_ExperimentID, iProverExperimentID),
    FOREIGN KEY (HLP_ExperimentID) REFERENCES HLP_Experiment(ExperimentID),
    FOREIGN KEY (iProverExperimentID) REFERENCES Experiment(ExperimentID)
);


CREATE TABLE HLP_ProblemCollectionExperiment (
    LoopNumber	 INT UNSIGNED NOT NULL,
    CollectionName   VARCHAR(256) NOT NULL,
    EndTime 	 DATETIME,
    PRIMARY KEY (LoopNumber, CollectionName),
    FOREIGN KEY (CollectionName) REFERENCES Collection(CollectionName)
);


CREATE TABLE HLP_RunMeasurement (
    HLPRunID            INT UNSIGNED NOT NULL,
    ExperimentID        INT UNSIGNED NOT NULL,
    DefaultHeuristic    VARCHAR(256),
    GlobalHeuristics    VARCHAR(256),
    SolvedUnion         INT(11),
    SolvedInterAvgTime  DECIMAL(8,3),
    SolvedComplement    INT(11),
    SolvedBestHeuristic INT(11),
    Primary KEY (HLPRunID, ExperimentID),
    FOREIGN KEY (ExperimentID) REFERENCES HLP_Experiment(ExperimentID),
    FOREIGN KEY (HLPRunID) REFERENCES HLP_Run(HLPRunID)
);


