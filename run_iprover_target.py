#!/usr/bin/env python3
"""
Script for running iProver as a target function with a given set of options on a set of problems.
"""
import sys
import time
import traceback
from enum import Enum
from typing import Dict, Tuple, Optional, List

import config
from hos.logger import get_target_logger
from hos.experiment import run_cluster_experiment
from hos.target.validity import check_heuristic_validity, handle_invalid_heuristic
from hos.target.viability import check_heuristic_viability, handle_nonviable_heuristic
from hos.target.translate_to_iprover import translate_argument_dict_to_heuristic
from hos.target.heuristic import Heuristic
from hos.experiment_config import get_local_experiment_config

from hos.target.eval_cost import compute_quality_scores
import hos.database as db
from hos.utils import read_experiment_parameters

# Get the logger
log = get_target_logger()


# TODO could use the seed from SMAC and use it do generate random wait times?
# e.g. check if seed is there first


class TargetStatus(str, Enum):
    """Class for representing the status of the target function run."""

    # The extra statuses are from the SMAC documentation. Could be implemented if needed.

    # RUNNING = 0 # In case a job was submitted, but it has not finished.
    SUCCESS = "SUCCESS"
    CRASHED = "CRASHED"
    # TIMEOUT = 3
    # MEMORYOUT = 4


def report_target_run(status: TargetStatus, runtime: float, quality: int) -> None:
    # Report result to SMAC
    result = f"cost={quality}; runtime={runtime}; status={status};"
    print(result)
    log.info(result)
    # print(f"Result of algorithm run: {status}, {runtime}, 0, {quality}, 0")


def get_parameters_from_input(input_params: List[str]) -> Dict[str, str]:
    # TODO need to have a test for this now
    log.debug("SMAC provided options: " + " ".join(input_params))

    # Build the param dict
    param_dict = {}
    for param in input_params:
        k, v = param.split("=")
        param_dict[k[2:]] = v  # Insert value without dashes

    if "seed" in param_dict:
        # SMAC provides a seed option which is not used
        del param_dict["seed"]

    # TODO might modify this with a wait/idle comment if using parallel

    return param_dict


def get_experiment_machines() -> Tuple[str, bool]:
    # If experiments are run in parallel - the machines must be locked from the database
    parallel = db.smac.check_experiment_is_parallel()
    if parallel:
        log.debug("Current experiment is parallel")
        machines = db.machines.lock_available_machine_set()
    else:
        machines = config.MACHINES
    log.debug(f"Acquired machine set: {machines}")
    return machines, parallel


def post_target_run_cleanup(param_dict: Dict[str, str], experiment_id: int, runtime: float) -> None:
    # Insert the parameter string into the DB to use for warm start
    db.smac.insert_param_string(param_dict, experiment_id)

    # Compute all the different quality measures of the run
    metric, quality = compute_quality_scores(experiment_id)
    log.debug(f"Quality metric: {metric} score: {quality}")

    # Report the current WrapperRun to the db
    db.smac.insert_wrapper_run(experiment_id, quality, runtime)

    # Print output to report to SMAC
    report_target_run(TargetStatus.SUCCESS, runtime, quality)


def run_target_evaluation(heuristic: Heuristic, exp_config_file: str, problem_set: str, machines: str) -> int:
    """Runs the heuristic on iProver and check validity, viability and effectiveness."""

    # Check if the configuration is a valid heuristic
    valid_check = check_heuristic_validity(heuristic)
    if not valid_check.is_valid:
        log.info("Heuristic is invalid")
        experiment_id = handle_invalid_heuristic(exp_config_file, problem_set, machines, valid_check)
        return experiment_id

    # Check if the heuristic is viable on the problem cluster
    viable = check_heuristic_viability(heuristic)
    if not viable:
        log.info("Heuristic was not viable")
        experiment_id = handle_nonviable_heuristic(exp_config_file, problem_set, machines)
        return experiment_id

    # Run the iprover experiment
    experiment_id = run_cluster_experiment(exp_config_file, problem_set, machines)
    return experiment_id


def main() -> None:
    runtime: float = 0.0
    experiment_is_parallel: bool = False
    machines: Optional[str] = None

    try:
        # Get experiment parameters
        exp_params = read_experiment_parameters()
        problem_collection = str(exp_params["problem_collection"])
        wc_local = int(exp_params["wc_local"])
        log.setLevel(exp_params["loglevel"])

        # Get the heuristic parameters from the input provided by SMAC
        param_dict = get_parameters_from_input(sys.argv[1:])

        # Create the heuristic for this experiment - make a copy to preserve dict for insertion
        heuristic = translate_argument_dict_to_heuristic(param_dict.copy())

        machines, experiment_is_parallel = get_experiment_machines()

        # Get the experiment config file for this experiment
        exp_config = get_local_experiment_config(prover_options=heuristic.get_prover_options(wc_local))
        exp_config_file = exp_config.to_file()

        # Start the timer
        start_time = time.perf_counter()
        experiment_id = run_target_evaluation(heuristic, exp_config_file, problem_collection, machines)
        # End the timer and set status to success
        end_time = time.perf_counter()
        runtime = end_time - start_time
        log.info(
            f"Experiment status: {TargetStatus.SUCCESS} with id {experiment_id} and runtime: {runtime:.2f}"
        )

        # Compute the cost, clean up, and report to SMAC
        post_target_run_cleanup(param_dict, experiment_id, runtime)

    except Exception as err:
        log.error(str(err))
        log.error(str(traceback.format_exc()))

        report_target_run(TargetStatus.CRASHED, runtime, 0)
    finally:
        if experiment_is_parallel and machines:
            db.machines.release_set_of_machines(machines)
            log.debug(f"Released set of machines: {machines}")


if __name__ == "__main__":
    main()
