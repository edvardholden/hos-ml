SHELL:=/bin/bash

# from https://github.com/Versent/simple-project-templates/blob/master/python/cookiecutter/simple-python-template/%7B%7Bcookiecutter.project_slug%7D%7D/Makefile
.PHONY: clean clean-test clean-pyc clean-build docs help
.DEFAULT_GOAL := help

PEX := pex
VIRTUALENV := python -m venv
PIP := .venv/bin/pip
SCRIPT := {{ cookiecutter.script_name }}


define BROWSER_PYSCRIPT
import os, webbrowser, sys

try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

lint: ## check style with flake8
	black tests src run_iprover_target.py run_optimiser.py

check: ## Perform all the checks
	pytest
	mypy src run_iprover_target.py run_optimiser.py
	flake8 src run_iprover_target.py run_optimiser.py

test: ## run tests quickly with the default Python
	pytest --cov-report term

test-all: ## run tests on every Python version with tox
	tox

coverage: ## check code coverage quickly with the default Python
	pytest --cov-report=html
	$(BROWSER) htmlcov/index.html

dist: clean ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

install: clean ## install the package to the active Python's site-packages
	python setup.py install

.PHONY: venv
.PHONY: venv-act
.PHONY: venv-deact

venv-act: ## Activate the virtual environment
	@echo "Run: "
	@echo "$ source .venv/bin/activate"


venv-deact: ## Deactivate the virtual environment
	@echo "Run: "
	@echo "$ deactivate"

venv:  ## Create and activate a virtual environment
	$(VIRTUALENV) .venv
	$(PIP) install -r requirements.txt
	$(PIP) install -r requirements_dev.txt
	$(PIP) install -e .
	#	venv-act


.PHONY: requirements.txt
requirements.txt:
	@echo 'Installing into a clean virtualenv'
	$(eval TMPDIR := $(shell mktemp -d))
	$(VIRTUALENV) $(TMPDIR)
	$(TMPDIR)/bin/pip install --upgrade -e .
	@echo 'Writing out new requirements.txt based on setup.py'
	$(TMPDIR)/bin/pip freeze -r requirements.txt | grep -v '^-e' > requirements.txt
	@echo 'Wrote new requirements.txt'
	rm -rf $(TMPDIR)
