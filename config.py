from pathlib import Path
import logging
from typing import Dict

log = logging.getLogger()

# TEST_PROBLEM = "test_problems/smac_test_heuristic_problem_fof.p" #
# TEST_PROBLEM = os.path.join(config.dpath, "wrappers/test_problems/smac_test_heuristic_problem.p")
# TEST_PROBLEM = "res/test_problems/ARI062_1_unsat_z3.p"  # TODO should be selected differently?
TEST_PROBLEM = "res/test_problems/PUZ001+1.p"  # TODO should be selected differently?

CLAUSIFIER_PATH = str(Path("~/bin/vclausify_rel").expanduser())
CLAUSIFIER_OPTIONS = ""  # " --show_fool true --input_syntax smtlib2 "

# Static options that are always added to an online experiment.
STATIC_EXPERIMENT_OPTIONS: Dict[str, str] = {
    "schedule": "none",
    "out_options": "none",
    "proof_out": "false",
    "stats_out": "none",
    "preprocessed_out": "false",
}

TARGET_FUNCTION = "run_iprover_target.py"

LOG_LEVEL = logging.INFO  # The default log level

# TODO this is the overall machine - need to set it in a better way
MACHINES = "vip-hlp-evaluate"

PROBLEMS_IN_TFF: bool = False
PROBLEM_COLLECTION_IS_LTB: bool = False
INCLUDE_INCORRECT: bool = False

invalid_heuristic_counter = "invalid_heuristic_counter.log"
invalid_heuristic_log = "invalid_heuristic_log.log"
